function USER_BeforePageLoad(){	
	var docX:NotesXspDocument = document1;
	AppForm_BeforePageLoad(document1);	
	if(docX.isNewNote()){
		docX.removeItem('DocKey');	// This is generated in Query Save
	}
}

function USER_QuerySave(){
	
	var docX:NotesXspDocument = document1;	
	if(USER_IsInterpreter() == false){docX.removeItem('USER_Languages')}	
	
	var strFN = @Trim(GetValue('USER_FirstName'));
	var strLN = @Trim(GetValue('USER_LastName'));
	docX.replaceItemValue('USER_FirstName', strFN);
	docX.replaceItemValue('USER_LastName', strLN);
	strFullName = strFN + ' ' + strLN;
	docX.replaceItemValue('USER_FullName', strFullName);
	
	if(IsAuth_Domino()){
		
		//Create Doc Key
		if(docX.hasItem('DocKey') == false){
		
			var strPre = @UpperCase(@Left(strFN, 1) + @Left(strLN, 1));
			var strFormula = 'Form = "' + docX.getForm() + '" & @Left(DocKey;2) = "' + strPre +'"';
			var dc:NotesDocumentCollection = docX.getParentDatabase().search(strFormula, null, 0);
			var intPos = 0;
			if(dc.getCount()>0){
				var doc:NotesDocument = dc.getFirstDocument(); var intTemp;
				while (doc !=null){
					intTemp = @TextToNumber(@RightBack(doc.getItemValueString('DocKey'), 2));
					if(intTemp > intPos){intPos = intTemp}
					doc = dc.getNextDocument(doc);
				}
			}
			
			intPos = intPos + 1;
			var strPatt = GetKeyword('USER_DocKey_Pattern');
			var docKey = strPatt + intPos.toString();		
			docKey = strPre + @Right(docKey, strPatt.length);
			docX.replaceItemValue('DocKey', docKey);
		}	
			
		//New NAB Document
		if(docX.hasItem('USER_NotesName') == false){
					
			var docNAB:NotesDocument = GetDB_NAB().createDocument();
			docNAB.replaceItemValue('Form', 'Person');
			docNAB.replaceItemValue('Type', 'Person');
			docNAB.replaceItemValue('LastName', GetValue('DocKey'));
			docNAB.replaceItemValue('FullName', "CN=" + GetValue('DocKey') + "/O=" + GetKeyword("USER_Domain"))
			docX.replaceItemValue('USER_EffectiveUserName', "CN=" + GetValue('DocKey') + "/O=" + GetKeyword("USER_Domain"))
			docNAB.replaceItemValue('PhoneNumber', @Trim(GetValue('USER_Phone')));
			docNAB.replaceItemValue('InternetAddress', GetValue('USER_Email'));
			docNAB.replaceItemValue('MailAddress', GetValue('USER_Email'));
			docNAB.replaceItemValue('MailSystem', '3');
			docNAB.replaceItemValue('HTTPPassword', GetValue('USER_Password'));
			docNAB.replaceItemValue('CustomPassword', GetValue('USER_Password'));
			docNAB.computeWithForm(true, false);
			docNAB.save(true, false);
			docNAB.recycle();			
			docX.replaceItemValue('USER_NotesName', GetValue('DocKey') + GetKeyword("USER_Domain"));
		}
		
		//Check Password change
		if(docX.isNewNote() == false){	
			if(docX.getDocument().getItemValueString('USER_Password') != GetValue('USER_Password')){					
				//Get NAB Document for this user
				var docNAB:NotesDocument = GetDB_NAB().getView('($Users)').getDocumentByKey(GetValue('DocKey'), true);
				if(docNAB == null){
					Global_LogError("ERROR: Could not find your login information. Please contact IT for asistance."); 
					return false
				}			
				//All Good Update the Document
				docNAB.replaceItemValue("HTTPPassword", GetValue('USER_Password'));
				docNAB.replaceItemValue("CustomPassword", GetValue('USER_Password'));	
				docNAB.computeWithForm(true, false);
				docNAB.save(true, false);
				docNAB.recycle();
			}
		}		
	}
	
	//Search if user may have registered as Requestor
	if(GetKeyword("Global_SelfRegister") == "YES" & docX.isNewNote()){		
		var vwREQU:NotesView = GetDB_Database("REQU").getView('all_byDocKey');
		var docREQU:NotesDocument = vwREQU.getDocumentByKey(docX.getItemValue('DocKey'), true);
		if(docREQU != null){Global_RemoveDocument(docREQU)}				
	}
		
	USER_LEAV_Save();		
}

function USER_PostSave(){USER_Hours_NonStandard(true); USER_Leaves(true)}

function USER_Role_Secondary_Options(){
	var docX:NotesXspDocument = document1;
	var strRole = GetValue('USER_Role_Primary');
	if(IsNullorBlank(strRole)){return null}
	var roles = GetKeyword('USER_Role');
	roles = @ReplaceSubstring(roles.join('~'), strRole, '');
	return @Trim(roles.split('~'))
}

function USER_IsInterpreter(){
	var docX:NotesXspDocument = document1;
	var vRole = GetValue('USER_Role_Primary');
	if(vRole == "Interpreter"){return true}
	vRole = docX.getItemValue('USER_Role_Secondary');
	if(vRole.indexOf('Interpreter')>=0){return true}
	return false
}

function USER_SecondaryRole_Show(){
	if(document1.isNewNote()){return document1.getItemValueString('USER_Role_Primary') != ''}
	if(document1.isEditable()){return true}
	return document1.getItemValue('USER_Role_Secondary').join('') != ''	
}

function USER_DeActivate(){
	var doc:NotesDocument = document1.getDocument();
	doc.replaceItemValue('DEACTIVE', 'YES');
	doc.save(true, false);
}

function USER_SearchUser_Show(){
	if(IsAuth_Domino()){return false}
	var docX:NotesXspDocument = document1;
	if(document1.hasItem('DocKey')){return false}
	return true
}

function USER_SearchUser_OnSelect(){
	
	var docX:NotesXspDocument = document1;
	var strSelectedValue = GetValue('USER_Search_LDAP');
	var vValues = strSelectedValue.split("; ");
	vValues = @Trim(@Right(vValues, ":"));
	
	var docExist:NotesDocument = GetDB_Database('USER').getView('all_byDocKey').getDocumentByKey(vValues[3], true);
	if(docExist != null){
		var vRoles = docExist.getItemValue("USER_Role_Secondary");
		if(IsNullorBlank(vRoles)){
			vRoles = docExist.getItemValue("USER_Role_Primary");
		} else {
			vRoles.push(docExist.getItemValueString("USER_Role_Primary"))
		}
		docX.replaceItemValue('USER_Search_LDAP', '');
		view.postScript("Notify_Warning('ERROR: \"" + vValues[1] + vValues[2] + "\" - already registered as " + vRoles.join(', ') + "')");
		return
	}
		
	//Assign values to document
	docX.setValue("DocKey", vValues[3]);
	docX.setValue("USER_FirstName", vValues[0]);
	docX.setValue("USER_LastName", vValues[1]);
	docX.setValue("USER_Email", vValues[2]);
	
	var strUserName = vValues[4];
	strUserName = @ReplaceSubstring(strUserName,"\\","");
	strUserName = @ReplaceSubstring(strUserName, ",OU", "/OU");
	strUserName = @ReplaceSubstring(strUserName, ",DC", "/DC");
	strUserName = @ReplaceSubstring(strUserName, ",CN", "/CN");	
	docX.setValue("USER_EffectiveUserName", strUserName);
}

var gbl_DSplit = '~@~'; var gblFSplit = '|@|';
var gbl_LeaveFileds = ['DocKey', 'From_Date', 'From_Time', 'From_DateTime', 'Till_Date', 'Till_Time', 'Till_DateTime', 'Comments'];

function USER_LEAV_Data(){
	
	var docX:NotesXspDocument = document1;
	if(docX.isNewNote() == false){			
		var vw:NotesView = docX.getParentDatabase().getView('USER_LEAV_Data');
		var nvec:NotesViewEntryCollection = vw.getAllEntriesByKey(GetValue('DocKey'), true);
		if(nvec.getCount() > 0){			
			var strData = '';
			var nve:NotesViewEntry = nvec.getFirstEntry();
			while (nve != null){				
				strData += (nve.getColumnValues().elementAt(2) + gbl_DSplit)				
				nve = nvec.getNextEntry(nve);
			}			
			return @LeftBack(strData, gbl_DSplit);			
		}	
	}
}

function USER_LEAV_AddUpdate(){
	
	var docX:NotesXspDocument = document1;	
	var vData = getComponent('USER_LEAV').getValue();
	if(IsNullorBlank(vData) == false){vData = vData.split(gbl_DSplit)} else {vData = []}
	
	var strData = ''; var strValue; var strDocKey;
	for(var i=0; i<gbl_LeaveFileds.length; i++){
		strValue = GetValue('USER_LEAV_' + gbl_LeaveFileds[i]);
		if(gbl_LeaveFileds[i] == 'DocKey'){strDocKey = strValue}
		if(strValue == '00:00'){strValue = ''}
		strData += (strValue + gblFSplit);
	}
	strData = @LeftBack(strData, gblFSplit);
	
	if(@Left(strDocKey, 3) == 'NEW'){
		vData.push(strData);	
	} else {
		for(var i=0; i<vData.length; i++){if(vData[i].indexOf(strDocKey) >=0){vData[i] = strData}}
	}	
	//print(vData)
	getComponent('USER_LEAV').setValue(vData.join(gbl_DSplit));	
}

function USER_LEAV_HTML(){
	
	var docX:NotesXspDocument = document1;
	
	var strHTMLTemplate = '<div class="row"><div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">';
	strHTMLTemplate += '<div class="col-md-1 col-sm-1 col-xs-1">COL1</div>';
	strHTMLTemplate += '<div class="col-md-3 col-sm-3 col-xs-3">COL2</div>';
	strHTMLTemplate += '<div class="col-md-3 col-sm-3 col-xs-3">COL3</div>';
	strHTMLTemplate += '<div class="col-md-3 col-sm-3 col-xs-3">COL4</div>';
	strHTMLTemplate += '<div class="col-md-2 col-sm-2 col-xs-2">COL5</div></div></div>';
	
	var strHTML_EditDelete = '<span id="@ID@_actionBtn">';	
	if(docX.isEditable()){
		strHTML_EditDelete += '<a href="#" style="margin-right:5px" title="Edit Record" onclick="javascrtip:USER_LEAV_Edit(\'@ID@\')" class="xspLink">';
		strHTML_EditDelete += '<span class="glyphicon glyphicon-edit" /></a>';
		strHTML_EditDelete += '<a href="#" title="Delete Record" onclick="javascrtip:USER_LEAV_Delete_Confirm(\'@ID@\')" class="xspLink">';
		strHTML_EditDelete += '<span class="glyphicon glyphicon-trash" /></a>';
	}	
	strHTML_EditDelete += '</span>';
	
	var vData = getComponent('USER_LEAV').getValue();
	if(IsNullorBlank(vData)){
		return '<div class="row"><div class="col-md-12 col-sm-12 col-lg-12 col-xs-12"><span class="control-label">No records available</span></div></div>';
	}
	
	var vTemp; var strTemp; var strHTMLReturn = '';
	
	//Header Row
	strTemp = strHTMLTemplate;
	strTemp = @ReplaceSubstring(strTemp, 'COL1', '<span class="control-label">#</span>');
	strTemp = @ReplaceSubstring(strTemp, 'COL2', '<span class="control-label">From Date/Time</span>');
	strTemp = @ReplaceSubstring(strTemp, 'COL3', '<span class="control-label">Till Date/Time</span>');
	strTemp = @ReplaceSubstring(strTemp, 'COL4', '<span class="control-label">Comments/Reason</span>');
	strTemp = @ReplaceSubstring(strTemp, 'COL5', '');
	strHTMLReturn += strTemp;
	
	vData = vData.split(gbl_DSplit);
	for(var i=0; i<vData.length; i++){
		vTemp = vData[i].split(gblFSplit);
		strTemp = strHTMLTemplate;
		strTemp = @ReplaceSubstring(strTemp, 'COL1', '<span id="' + vTemp[0] + '_count">' + (i+1) + '</span>');
		strTemp = @ReplaceSubstring(strTemp, 'COL2', '<span id="' + vTemp[0] + '_dtFrom">' + vTemp[1] + ' ' + vTemp[2] + '</span>');
		strTemp = @ReplaceSubstring(strTemp, 'COL3', '<span id="' + vTemp[0] + '_dtTill">' + vTemp[4] + ' ' + vTemp[5] + '</span>');
		strTemp = @ReplaceSubstring(strTemp, 'COL4', '<span id="' + vTemp[0] + '_comments">' + vTemp[7] + '</span>');
		strTemp = @ReplaceSubstring(strTemp, 'COL5', strHTML_EditDelete);
		strTemp = @ReplaceSubstring(strTemp, '@ID@', vTemp[0]);		
		strHTMLReturn += strTemp;
	}	
	return strHTMLReturn;	
}

function USER_LEAV_Save(){
			
	var vData = getComponent('USER_LEAV').getValue();
	if(IsNullorBlank(vData)){return true}
	
	var docX:NotesXspDocument = document1;	
	for(var i=0; i<gbl_LeaveFileds.length; i++){docX.removeItem('USER_LEAV_' + gbl_LeaveFileds[i])}
	
	vData = vData.split(gbl_DSplit); var vTemp;
	var vDataNew = [];
	for(var i=0; i<vData.length; i++){
	
		vTemp = vData[i].split(gblFSplit);
		
		//New Document
		if(@Left(vTemp[0], 3) == 'NEW'){	
			
			var docNew:NotesDocument = GetDB_Database('USER').createDocument();
			docNew.replaceItemValue('Form', 'USER_LEAV');
			docNew.replaceItemValue('DocKey', AppForm_DocKey('USER_LEAV'));
			docNew.replaceItemValue('DocKey_Parent', GetValue('DocKey'));
			docNew.replaceItemValue('DocKey_CreatedBy', General_GetUser_DocKey());
			docNew.replaceItemValue('CreatedDate', session.createDateTime(@Today()).getDateOnly());
			docNew.replaceItemValue('CreatedBy', General_GetUser_FullName());
			for(var j=1; j<gbl_LeaveFileds.length; j++){docNew.replaceItemValue('USER_LEAV_' + gbl_LeaveFileds[j], vTemp[j])}
			docNew.save(true, false);
			vTemp[0] = docNew.getItemValueString('DocKey');
		
		} else {
			
			var nv:NotesView = GetDB_Database('USER').getView('all_byDocKey');
			
			//DELETE DOCUMENT	
			if(vTemp[0].indexOf('~DELETE~')>=0){			
				var strDocKey = @LeftBack(vTemp[0], '~DELETE~');
				var docD:NotesDocument = nv.getDocumentByKey(strDocKey, true);				
				if(docD != null){Global_RemoveDocument(docD)}
				
			} else {
			//Edit DOCUMENT	
				var docEdit:NotesDocument = nv.getDocumentByKey(vTemp[0], true);
				var bSave = false;
				for(var j=1; j<gbl_LeaveFileds.length; j++){
					if(docEdit.getFirstItem('USER_LEAV_' + gbl_LeaveFileds[j]).getText() != vTemp[j]){
						docEdit.replaceItemValue('USER_LEAV_' + gbl_LeaveFileds[j], vTemp[j]);
						bSave = true;
					}
				}
				if(bSave){
					docEdit.replaceItemValue("ModifiedDate", session.createDateTime(@Today()).getDateOnly());
					docEdit.replaceItemValue("ModifiedBy", General_GetUser_FullName());
					docEdit.save(true, false);
				}			
			}		
		}
		
		if(vTemp[0].indexOf('~DELETE~')<0){vDataNew.push(vTemp.join(gblFSplit))}		
	}
	getComponent('USER_LEAV').setValue(vDataNew.join(gbl_DSplit))	
}