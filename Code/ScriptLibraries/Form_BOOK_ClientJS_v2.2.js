function BOOK_CalculateActualTime ()
{
	var startTime = BOOK_Booking_GetField ("BOOK_ApptStartTime_Actual").split(":");
	var endTime = BOOK_Booking_GetField ("BOOK_ApptFinishTime_Actual").split(":");
	
	//date not set or not formatted right
	if (startTime.length<=1 | endTime.length<=1) {$('.fld_BOOK_ApptDuration_Actual').val("");return};
		
	var minDiff = endTime[1] - startTime[1];
	var hoursDiff = endTime[0] - startTime[0];
	
	hoursDiff = hoursDiff * 60;
	minDiff += hoursDiff;
	
	if (minDiff < 0) {
		//clear out endtime
		$('.fld_BOOK_ApptDuration_Actual').val("");
		$('.fld_BOOK_ApptFinishTime_Actual').val("");
		return;
	}
	$('.fld_BOOK_ApptDuration_Actual').val(minDiff);
	
}

function BOOK_NC_Reason_OnChange(strValue){	
	JQ_ShowHide("div_BOOK_NC_Reason_Other", strValue == "other" | strValue == "error" );
	JQ_ShowHide("div_BOOK_NC_Reason_Cancelled", strValue == "cancelled");
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterNotRequired", strValue == "interpreter_nr");
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterFTA", strValue == "interpreter_fta");
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterFTA_Other", (strValue == "interpreter_fta" && $('.fld_BOOK_NC_Reason_InterpreterFTA').val() == 'Other') ? true : false);	
	
}

function BOOK_NC_Reason_InterpreterFTA_OnChange(strValue){	
	JQ_ShowHide("div_BOOK_NC_Reason_InterpreterFTA_Other", strValue == "Other");	
}

function BOOK_Language_OnChange_CJS(){
	var selObj = $('.fld_BOOK_Interpreter_DocKey option');
	if(selObj.size() == 2){selObj.eq(1).prop('selected', true)} else {selObj.eq(0).prop('selected', true)}
}

function BOOK_Print(strUNID) {
	var strURL = 'xBOOK_Print.xsp?documentId='+ strUNID + '&action=openDocument';
	//cLog(strURL)
	var strName = '_blank';
	var strSpecs = "height=600px,width=900px,status=no,resizable=no,menubar=no,titlebar=no,toolbar=no";
	//cLog(strSpecs)
    var win = window.open(strURL, strName, strSpecs);
    win.print();
}

function Parent_Grid_Reload_Interpreter(){
	cLog('Parent_Grid_Reload_Interpreter Reload ....');
	//Grid without DateRange
	var gridKeys = ['GRID_INT_Incomplete', 'GRID_INT_Unallocated'];
	var gridObj;
	for(var i=0; i<gridKeys.length; i++){
		gridObj = parent.$('#listx_' + gridKeys[i]);
		if(gridObj.length > 0){parent.Grid_Generate(gridKeys[i])}
	}
	//Grid with DateRange for CRD & REQU & INT
	gridObj = parent.$('#listxGrid');
	if(gridObj.length > 0){parent.Grid_Generate_DateRange()}	
}

function Parent_Calendar_Reload(){
	var calObj = parent.$('#divCalendar');
	if(calObj.length > 0){calObj.fullCalendar( 'refetchEvents' )}		
}

function BOOK_Completed_OnClick(fld){
	var fldY = $("input[type=checkbox][name*='BOOK_Completed'][value='Yes']");
	var fldN = $("input[type=checkbox][name*='BOOK_Completed'][value='No']");
	var strValue = '';
	if(fldY.prop('id') == fld.id){	
		if(fldY.prop('checked')){
			fldN.prop('checked', false);	//Uncheck other if checked
			strValue = 'Yes';
			//if the actual start isn't filled in then default it to booking start
			if (BOOK_Booking_GetField('BOOK_ApptStartTime_Actual').length <= 1){
				var startDate = BOOK_Booking_GetField('BOOK_ApptStartTime_Text');
				//Check if start and finish date have AM/PM, convert it to 24 hour format
				if (moment(startDate, "h:mm A", true) || moment(startDate, "hh:mm A", true)) {
					startDate = moment(startDate, ["h:mm A"]).format("HH:mm");
				}
				$('.fld_BOOK_ApptStartTime_Actual').val(startDate);
			}
			
		}
	} else {
		if(fldN.prop('checked')){
			fldY.prop('checked', false);	//Uncheck other if checked
			strValue = 'No';
		}		
	}
	
	JQ_ShowHide("div_BOOK_Completed_Yes", strValue == "Yes");
	JQ_ShowHide("div_BOOK_Completed_No", strValue == "No")
}

function BOOK_Save_OnComplete(){
	cLog("Booking saved")
	if($('.BOOK_CheckDouble').length > 0){
		var strDbMsg = $('.BOOK_CheckDouble').val();
		if(strDbMsg != '' && strDbMsg != 'IGNORE'){
			SaveNotification_Close();
			ConfirmAction(strDbMsg, null, "$('.BOOK_CheckDouble').val('IGNORE'); $('.btnSave').click();");
			return
		}
	}	
	
	var strMsg = $('.BOOK_CheckAvailability').html();
	cLog(strMsg)
	if(strMsg == ''){
		SaveNotification_OnComplete(); 
		BOOK_Common_Save();
		return
	}
	
	//If strMsg is not blank, SJS QuerySave returns false, by which document is not saved
	SaveNotification_Close();
		
	//Check following conditions for Work Planner
	var bCheck1 = ($('.scInterperterOnly').html() == 'N');			//Coordinator or Manager or DBAdmin
	var bCheck2 = ($('#dlg_ChangeInterpreterHours').length > 0);	//Planner is enabled
	var bCheck3 = ($('.scDatePassed').html() == ''); 				//Booking to be of future date
	var bCheck4 = (strMsg.toLowerCase().indexOf('leave') < 0);		//No Leave on that day for Interpreter
	
	cLog(strMsg); //MeaningFull Message ~ strIName ~ Code ~ BookingDate ~ DateTimes
	var vMsg = strMsg.split('~');
		
	if(bCheck1 && bCheck2 && bCheck3 && bCheck4){			
		
		strMsg += '.<br/>Do you wish to modify working hours for this booking day ?';		
		$('#dlg_ChangeInterpreterHours_Date').html(BOOK_Booking_GetField('BOOK_ApptStartDate_Text'));
		$('#dlg_ChangeInterpreterHours_Msg').html(vMsg[0]);	//MeaningFull Message
		$('#dlg_ChangeInterpreterHours').modal();
		
		//if Length is greater than 5 it means Interpreter has some hours set for this day
		//Set Hours coming in with message
		console.log("interpretor hours change");
		console.log(vMsg)
		if(vMsg.length > 5){			
			$('.fld_NewTime_Start_1').val(vMsg[4]);
			$('.fld_NewTime_End_1').val(vMsg[5]);
			//Split hours present check
			$('.fld_NewTime_Start_2').val(((vMsg.length > 6) ? vMsg[6] : ''));
			$('.fld_NewTime_End_2').val(((vMsg.length > 6) ? vMsg[7] : ''));
			$('.fld_SplitHours').prop("checked", vMsg.length > 6);			
		}
		
		JQ_ShowHide('divTime_2nd', vMsg.length > 6);
		
	} else {
		Dialog_Message("Interpreter not available", vMsg[0]);			
	}
		
}

function BOOK_Common_Save(){
	UpdateFTIndex('xFTIndex_Agent.xsp');
	Parent_Calendar_Reload();
	Parent_Grid_Reload_Interpreter();
}

function BOOK_ChangeInterpreterHoursUpdate_Validate(){	
	
	var strBookDt = BOOK_Booking_GetField('BOOK_ApptStartDate_Text');
	if(strBookDt.length != 11){ChangeInterpreterHours_ErrorControl('Booking Date is not set properly'); return}
	
	var dtBookStart = moment(strBookDt,'DD MMM YYYY', true);
	if(dtBookStart.isValid() == false){ChangeInterpreterHours_ErrorControl('Date not valid as per moment class'); return}
	
	var strTime = BOOK_Booking_GetField('BOOK_ApptStartTime_Text');
	if(strTime.length != 5 || strTime.indexOf(':') != 2){ChangeInterpreterHours_ErrorControl('Booking Time is not set properly'); return}
	
	var strDuration = BOOK_Booking_GetField('BOOK_ApptDuration');
	if(strDuration.length == 0){ChangeInterpreterHours_ErrorControl('Booking Duration is not entered'); return}
	
	if(ChangeInterpreterHours_Validate(false, "", strTime, strDuration)){
		$('#dlg_ChangeInterpreterHours').modal('hide');
		$('.btnChangeInterpreterHoursUpdate_SJS').click();
	};	
}

function BOOK_Flexible_Show(){
	
	var divObj = $('.div_Flexible');
	if(divObj.length == 0){return}
	
	var fldObj = $('.fld_BOOK_ApptStartDate_Text');
	if(fldObj.length == 0){return}
	
	var strDate = fldObj.val();
	if(strDate.length == 0){return}
	if(strDate.length != 11){
		cLog('Function: BOOK_Flexible_Show, Error: Date not valid - ' + strDate)
		return
	}
	
	var dtBook = moment(strDate,'DD MMM YYYY', true);
	if(dtBook.isValid() == false){
		cLog('Function: BOOK_Flexible_Show, Error: Date not valid as per moment class')
		return
	}
	
	var strTime = $('.fld_BOOK_ApptStartTime_Text').val();
	if(strTime.length == 5 && strTime.indexOf(':') == 2){
		dtBook.hour(strTime.split(':')[0]);
		dtBook.minutes(strTime.split(':')[1]);
	} else {
		dtBook.hour(0);
		dtBook.minutes(01);
	}
	
	//cLog(dtBook)
	
	$.ajax({
		url: "agtTimeDiff?OpenAgent&dt=" + dtBook.format('YYYY~MM~DD~HH~mm'),
		cache: false,
		dataType: "json",
		success: function(data) {
			$.each( data, function( key, val ) {
				if(key == "hours"){					
					
					var intHours = parseInt($('.BOOK_Flexible_NoticeHours').html());					
					JQ_ShowHide("div_Flexible", parseFloat(val) <= intHours);
					
					//For Validation purpose
					if(parseFloat(val) <= intHours){
						$('.BOOK_Flexible_Validate').html('YES')
					} else {
						$('.BOOK_Flexible_Validate').html('NO')
					}
					var rbFld = $('input[type=radio][name*=\'BOOK_Flexible\']');	
					rbFld.prop('checked', false);
				}					
			});									
		}		
	});
}

function BOOK_Flexible_TimeRange_Show(){
	var strShow = $('input[type=radio][name*="BOOK_Flexible"][value=Yes]').is(":checked");
	cLog(strShow);
	JQ_ShowHide("div_FlexibleTimeRage", strShow);
}

function BOOK_DateInPastPrompt(bNewDoc){
	if(bNewDoc == false){return}
	var obj = $('.scAllowBookDateInPast').html();
	var strAllow = obj.substring(0, 3);
	if(strAllow.ignoreCase != 'YES'.ignoreCase){return}
	var vTemp = obj.split('~');
	if(vTemp.length != 2){cLog("Error Keyword-> AllowBookDateInPast not configured properly"); return}
	
	var fldObj = $('.fld_BOOK_ApptStartDate_Text'); 
	if(fldObj.length == 0){return}
	var strDate = fldObj.val();
	if(strDate.length == 0){return}
	if(strDate.length != 11){cLog('Function: BOOK_DateInPastPrompt, Error: Date not valid - ' + strDate); return}
	var dtBook = moment(strDate,'DD MMM YYYY', true);
	if(dtBook.isValid() == false){cLog('Function: BOOK_DateInPastPrompt, Error: Date not valid as per moment class'); return}
	
	if(dtBook.diff(moment(), 'days') <= -1){
		Notify_Warning(vTemp[1]);
	}
}

function BOOK_Booking_GetField(strField){
	//Used for Date Time and Duration....check for return value if used for any other field
	var objControl = $('.fld_' + strField);	
	if(objControl.length > 0){
		return strTrim(objControl.val());
	}
	var strReturn = strTrim($('.div_' + strField).html());
	console.log(strReturn);
	if(strReturn.indexOf('</span>')>=0){strReturn = strReturn.split('</span>')[1]}
	if(strReturn.indexOf('txt_Hide">')>=0){strReturn = strReturn.split('txt_Hide">')[1]}
	
	return strReturn
}

function BOOK_CheckRequestorEditControl(){
	//Return True if allowed to Edit
	var obj = $('.scAllowRequestorEditBookingControl').html();
	var strAllow = obj.substring(0, 3);
	if(strAllow == "YES"){return true}

	var vConfig = obj.split('~');
	if(vConfig.length != 3){cLog("Error Keyword-> AllowRequestorEditBookingControl not configured properly"); return false}
	
	var strDate = $($('.div_BOOK_ApptStartDate_Text')[0]).text();	
	if(strDate.length == 0 || strDate.length != 11){cLog('Function: BOOK_CheckRequestorEditControl, Error: Date not valid - ' + strDate); return false}
	
	var dtBook = moment(strDate,'DD MMM YYYY', true);
	if(dtBook.isValid() == false){cLog('Function: BOOK_CheckRequestorEditControl, Error: Date not valid as per moment class'); return false}
	
	var strTime = $($('.div_BOOK_ApptStartTime_Text')[0]).text();
	if(strTime.length == 0 || strTime.length != 5 || strTime.indexOf(':') != 2){cLog('Function: BOOK_CheckRequestorEditControl, Error: Time not valid - ' + strTime); return false}
	dtBook.hour(strTime.split(':')[0]);
	dtBook.minutes(strTime.split(':')[1]);
	
	var intHoursDiff = dtBook.diff(moment(), 'hours');
	var intAllowedHours = parseInt(vConfig[1]);
	if(intHoursDiff > intAllowedHours){
		return true
	}
	
	Dialog_Message("SYSTEM MESSAGE", vConfig[2]);
	return false;
		
}

function BOOK_RequestCancellation_CJS(){	
	if(BOOK_CheckRequestorEditControl() == false){return}
	$('#dlg_CancelReqBooking').modal()	
}

function BOOK_Requestor_EditOnClick(){	
	if(BOOK_CheckRequestorEditControl() == false){return}
	$('.btnEdit_Requestor_SJS').click();
}