function TypeAhead_JSON(){
	
	var externalContext = facesContext.getExternalContext();
	var writer = facesContext.getResponseWriter();
	var response = externalContext.getResponse();
 
	// Set content type
	response.setContentType("application/json");
	response.setHeader("Cache-Control", "no-cache");
	
	var out:com.primaxis.ViewColumn = new com.primaxis.ViewColumn(session);
	
	var strType = param.TAType; var strJSON = "";
	var strItemAppend = param.ItemApp;
	var strTAQuery = param.taQuery;
	var strForm = param.Frm;
	var intPos = -1;
	var intMaxCount = @TextToNumber(GetKeyword('Global_TypeAhead_ResultCount'));
	
	if(@IsMember(strType, ['Site', 'Language', 'AgencyName', 'InterpreterName'])){
		
		var dataValues = eval('TypeAhead_' + strType + '_List()');
		if(!IsNullorBlank(strItemAppend)){dataValues.push(strItemAppend)}
		dataValues = @Unique(dataValues);
		dataValues = out.JSON_Handle_SpecialCharacters(@Implode(dataValues, '.!.'))
		
		strJSON = '["' + @ReplaceSubstring(dataValues, '.!.', '", "') + '"]';
	
	} else if(strType == "Clinic"){
		
		//This call is from Booking or Reports
		//Booking can have site and location, while Reports will have only be clinic
		var vQuery = ['FIELD CLN_Clinic_Name CONTAINS "*' + strTAQuery + '*"'];
				
		if(strForm == 'BOOK'){			
			intPos = 0;			
			//if Site is present
			if(param.Site != null){vQuery.push('[CLN_Site_Code] = "' + param.Site + '"')}
			//if Location is present (Location only is not possible, since we set site also when choosing location)
			//if(param.Loc != null){vQuery.push('[CLN_Location_Name] = "' + param.Loc + '"')}						
		} 
		
		var strQuery = vQuery.join(' AND ');
		//print('Typeahead Query - ' + strQuery);
		var nvw:NotesView = GetDB_Database('CLN').getView('CLN_TA_Clinics');
		strJSON = out.getViewColumnValue_TypeAhead(nvw, strQuery, intPos, intMaxCount);
	
	} else if(strType == "Location"){
			
		//This call is from Booking
		var vQuery = ['FIELD CLN_Location_Name CONTAINS "*' + strTAQuery + '*"'];
		
		//if Site is present
		if(param.Site != null){vQuery.push('[CLN_Site_Code] = "' + param.Site + '"')}
		var strQuery = vQuery.join(' AND ');
		//print('Typeahead Query - ' + strQuery);
		var nvw:NotesView = GetDB_Database('CLN').getView('CLN_TA_Locations');
		
		strJSON = out.getViewColumnValue_TypeAhead(nvw, strQuery, intPos, intMaxCount);	
		
	} else if(strType == "URN") {
		/**
		 * Below if condition added by Baljit on 20 May 2019
		 * Based on KW to fetch patient data from patient database
		 */
		var searchPatientDB = (GetKeyword("SearchPatientFromPatientDB").equalsIgnoreCase("Yes"))? true:false;
		if (searchPatientDB) {
			strForm = 'Patient';
			var nvw:NotesView = GetDB_Database('Patient').getView('List_URN');	
			var strQuery = "FIELD UrNumber CONTAINS *KEY*";
		} else {
			var nvw:NotesView = GetDB_Database('BOOK').getView('List_URN');
			//var strQuery = "FIELD BOOK_PatientUR CONTAINS KEY* | FIELD BOOK_PatientLastName CONTAINS KEY* | FIELD BOOK_PatientFirstName CONTAINS KEY*";
			var strQuery = "FIELD BOOK_PatientUR CONTAINS *KEY*";
		}
		
		strQuery = @ReplaceSubstring(strQuery, "KEY", strTAQuery);
		
		strJSON = out.getViewColumnValue_TypeAhead(nvw, strQuery, -1, intMaxCount);
		
	} else if(strType == "ADUSER"){
		
		var ldap:com.primaxis.LDAPConnectAD = new com.primaxis.LDAPConnectAD(session);
		var strSearchString = @ReplaceSubstring(GetKeyword('LDAP_User_SearchString'), 'SEARCH_KEY', strTAQuery);
		var userList = ldap.SearchUser(strSearchString);
		
		if(userList.length == 0){
			strJSON = []
		} else {
		
			var limit = Math.min(userList.length, intMaxCount);
			var newList = [];
			
			for (j=0; j<userList.length; j++) {		
				//newList.push("ID: " + userList[j][0] + "; First Name: " + userList[j][1] + "; Last Name: " + userList[j][2] + "; Email: " + userList[j][3]);
				newList.push("First Name: " + userList[j][1] + "; Last Name: " + userList[j][2] + "; Email: " + userList[j][3] + "; ID: " + userList[j][4] + "; DN: " + userList[j][5]);
			}
			
			newList = out.JSON_Handle_SpecialCharacters(@Implode(newList, '.!.'))
			strJSON = '["' + @ReplaceSubstring(newList, '.!.', '", "') + '"]';
			
		}
	}
	
	writer.write(strJSON);
	writer.endDocument();
}


function TypeAhead_Language_List(){
	return GetAliasedKeyword('LanguageList')
}
function TypeAhead_Site_List() {return @Left(GetKeyword("BOOK_Site"), "|")}
function TypeAhead_AgencyName_List() {return @DbColumn(GetDB_FilePath('AGN'), 'AGN_NameList', 1)}
function TypeAhead_InterpreterName_List() {return @DbColumn(GetDB_FilePath('USER'), 'USER_Interpreters', 1)}

function TypeAhead_Multiple_Add(strLabel){
	
	var docX:NotesXspDocument = compositeData.dataSource;
	var strFld = compositeData.field_Name;	
	var fldObj:com.ibm.xsp.component.xp.XspInputText = getComponent('fld_AddValue');
	var strValue = @Trim(fldObj.getValue());
	fldObj.setValue(null);
		
	var vExistVals = docX.getItemValue(strFld);
	//Its already added to list
	if(vExistVals.indexOf(strValue) >=0 ){
		view.postScript("Notify_Warning('ERROR: <strong>" + strValue + "</strong> already added to " + strLabel + "')"); return
	}
	
	vExistVals.push(strValue)					
	docX.replaceItemValue(strFld, @Trim(vExistVals));
	
	TypeAhead_Multiple_DIV_StyleClass(docX, strFld, 'has-success', 'has-error');
}

function TypeAhead_Multiple_Remove(strValue){
	
	var docX:NotesXspDocument = compositeData.dataSource;
	var strFld = compositeData.field_Name;
	var vCodeList:java.util.Vector = docX.getItemValue(strFld);
	
	var intPos = vCodeList.indexOf(strValue);
	var bNotify = false;
	if(intPos >=0){vCodeList.remove(intPos); bNotify = true;}
	
	if(vCodeList.join('').length == 0){
		vCodeList = null;		
		TypeAhead_Multiple_DIV_StyleClass(docX, strFld, 'has-error', 'has-success');
	}
	docX.replaceItemValue(strFld, vCodeList);	
	if(bNotify){view.postScript("Notify_Info('" + strValue + " removed')")}	
}

function TypeAhead_Multiple_DIV_StyleClass(docX, strFld, strClassAdd, strClassRemove){
	
	if(@Implode(GetKeyword(docX.getForm() + "_ValidationJson"), "").indexOf('"fldName" : "' + strFld + '"') >=0){
		var x:com.ibm.xsp.component.xp.XspDiv = getComponent('divNameMultiple');
		var strStyleClass = x.getStyleClass();		
		if(strStyleClass.indexOf(strClassRemove)>=0){strStyleClass = @ReplaceSubstring(strStyleClass, strClassRemove, '')}
		if(strStyleClass.indexOf(strClassAdd)<0){strStyleClass += ' ' + strClassAdd}		
		x.setStyleClass(strStyleClass)
	}	
}
