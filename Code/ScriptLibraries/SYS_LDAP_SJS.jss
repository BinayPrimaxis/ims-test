function GetUser(){
	
	var strSearchString = GetKeyword('LDAP_User_SearchString');
	var strSName = getComponent('iName').getValue();
	//print("Searching for name ->" + strSName)
	strSearchString = @ReplaceSubstring(strSearchString, 'SEARCH_KEY', strSName)
	print("Search String for LDAP ->" + strSearchString)
	
	var ldap:com.primaxis.LDAPConnectAD = new com.primaxis.LDAPConnectAD(session);
	var userList = ldap.SearchUser(strSearchString);
	print("userList length -> " + userList.length)
	getComponent("result").setValue("userList length -> " + userList.length);
	
	if(userList.length > 0){
		
		var strHTML = "Total Users Found: " + userList.length + "";
		
		for(var i=0; i<userList.length; i++){
			strHTML += "<br/><br/>";
			var vTemp = userList[i];
			strHTML += vTemp.join("<br/>")
			
		}
		
		strHTML += "<br/><br/>"
		
	}
	
	getComponent("result").setValue(strHTML);
}