function CLN_GetFieldLabel (strFieldName)
{
	CLN_SetFieldLabelsScope();
	return !applicationScope.containsKey("CLN_Labels") || !applicationScope.get("CLN_Labels")[strFieldName] ? strFieldName : applicationScope.get("CLN_Labels")[strFieldName]
}

function CLN_SetFieldLabelsScope ()
{
	if (applicationScope.containsKey ("CLN_Labels")) {return}
	var labels = fromJson(GetKeyword ("CLN_Dynamic_FieldLabels"));
	applicationScope.put ("CLN_Labels", labels);
}

function CLN_Location_TypeaheadURL(){	
	var docX:NotesXspDocument = document1;
	var strSite = GetValue('CLN_Site_Code');
	var strURL = './xJson_Data.xsp?TAType=Location';
	if(strSite != ''){strURL += '&Site=' + strSite;}
	strURL += '&taQuery=';
	//print(strURL)
	return strURL
}

function CLN_Location_OnChange(){
	//check if value has site in it, then set site, else return
	//Sample Value: A Corridor - Outpatient Clinic 3 (Site: BHS)
	var docX:NotesXspDocument = document1;
	var strValue = docX.getItemValueString('CLN_Location_Name');
	if(strValue.indexOf('(Site:') >=0){
		docX.replaceItemValue('CLN_Location_Name', @Trim(@Left(strValue, '(Site:')));		
		docX.replaceItemValue('CLN_Site_Code', @Trim(@Left(@Right(strValue, '(Site:'), ')')));
	}	
}

function CLN_QuerySave(){
	
	var docX:NotesXspDocument = document1;
	var msgObj = getComponent('Form_SaveErrorMessage'); 
	
	//Check for Duplication
	var strFormula = 'Form = "CLN" & CLN_Site_Code = "' + GetValue('CLN_Site_Code') + '"';
	strFormula += ' & CLN_Location_Name = "' + GetValue('CLN_Location_Name') + '"';
	strFormula += ' & CLN_Clinic_Name = "' + GetValue('CLN_Clinic_Name') + '"';
	strFormula += ' & @Text(@DocumentUniqueID) != "' + docX.getDocument(false).getUniversalID() + '"'
		
	var dc:NotesDocumentCollection = docX.getParentDatabase().search(strFormula, null, 0);
	
	if(dc.getCount() > 0){
		if(docX.isNewNote()){
			msgObj.setValue("Duplicate Clinic found under same Campus and Campus Area");	
		} else {
			msgObj.setValue("No Values Changed or Duplicate Clinic found under same Campus and Campus Area");
		}
		//
		return false
	} else {
		msgObj.setValue('');
	}
	
	docX.replaceItemValue('CLN_Site_Name', KeywordLabelValue(GetValue('CLN_Site_Code'), 'BOOK_Site'))
			
}