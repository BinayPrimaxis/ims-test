Number.prototype.formatMoney = function(c, d, t){
    var n = this, 
    c = isNaN(c = Math.abs(c)) ? 2 : c, 
    d = d == undefined ? "." : d, 
    t = t == undefined ? "," : t, 
    s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))), 
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
 function Chart_JSON(){
	var externalContext = facesContext.getExternalContext();
	var writer = facesContext.getResponseWriter();
	var response = externalContext.getResponse();
	response.setContentType("application/json");
	response.setHeader("Cache-Control", "no-cache");
	var strKey = param.ChartKey;	
	if(IsNullorBlank(strKey)){
		writer.write("[]"); 
		writer.endDocument(); 
		print("ERROR:No KEY passed for JSON");
		return
	}
	writer.write(Chart_GetData(strKey));
	writer.endDocument();	
}

function Chart_GetData(strKey){
	
	var ndbData:NotesDatabase = GetDB_Database('BOOK');
	var nvChart:NotesView; var nvChart_1:NotesView;	
	
	if(ArchiveEnabled()){
		//Archive Database
		var dblYearMonth = @TextToNumber(GetKeyword('Global_BOOK_Data_YearMonth'));
		var ndbData_A:NotesDatabase = GetDB_BOOK_Archive();
		var nvChart_A:NotesView; var nvChart_1_A:NotesView;
	}	
	
	var strType = @Left(strKey, "_");
	var strRange = @Right(strKey, "_");
	var dtFirst:Date; var dtLast:Date;
	
	var vRange:Array = []; var vLabels:Array = [];
	
	if(strType == "Month"){
		
		nvChart = ndbData.getView("Chart_byMonth");
		nvChart_1 = ndbData.getView("Chart_byMonth_Type");
		
		if(ArchiveEnabled()){
			//Archive Database
			nvChart_A = ndbData_A.getView("Chart_byMonth");
			nvChart_1_A = ndbData_A.getView("Chart_byMonth_Type");
		}
		
		//Sample - 201508-201607
		var vTemp = strRange.split("-");
		dtFirst = @Date(@Left(vTemp[0], 4), @Right(vTemp[0], 2), 10);
		dtLast = @Date(@Left(vTemp[1], 4), @Right(vTemp[1], 2), 10);
		
		while (parseInt(I18n.toString(dtFirst, "yyyyMM")) <= parseInt(I18n.toString(dtLast, "yyyyMM"))){
			
			vLabels.push(I18n.toString(dtFirst, "MMM yy"));
			vRange.push(I18n.toString(dtFirst, "yyyyMM"));
			
			if(dtFirst.getMonth() == 11){
				dtFirst.setFullYear(dtFirst.getFullYear()+1, 0)
			} else {
				dtFirst.setMonth(dtFirst.getMonth()+1)
			}			
		}
				
	} else if(strType == "Day"){
		
		nvChart = ndbData.getView("Chart_byDay");
		nvChart_1 = ndbData.getView("Chart_byDay_Type");
		
		if(ArchiveEnabled()){
			var dblYearMonth = @TextToNumber(@Text(dblYearMonth) + '01');
			//Archive Database
			nvChart_A = ndbData_A.getView("Chart_byDay");
			nvChart_1_A = ndbData_A.getView("Chart_byDay_Type");
		}
		
		dtFirst = @Date(@Left(strRange, 4), @Right(strRange, 2), 1);
		
		
		var strMonth = @Month(dtFirst);	
		while (strMonth == @Month(dtFirst)){
			vLabels.push(I18n.toString(dtFirst, "dd"));
			vRange.push(I18n.toString(dtFirst, "yyyyMMdd"))
			dtFirst = @Adjust(dtFirst, 0, 0, 1, 0, 0, 0);
		}
	}
	
	nvChart.setAutoUpdate(false);
		
	var vTotal:Array = []; var vUnmet:Array = []; var vCompleted:Array = []; var vCancelled:Array = [];
	var vAgency:Array = []; var vInHouse:Array = []; 
	var strCat; var nv:NotesView; var nv_1:NotesView;
	
	//Loop to get data for the range
	for(var i=0; i<vRange.length; i++){
		strCat = vRange[i];
		
		//Check which database to connect
		if(ArchiveEnabled() & @TextToNumber(strCat)<dblYearMonth){
			nv = nvChart_A; nv_1 = nvChart_1_A;			
		} else {
			nv = nvChart; nv_1 = nvChart_1;
		}
		
		vTotal.push(getCountFromView(nv, strCat));		
		vUnmet.push(getCountFromView(nv, strCat + "\\" + strCat + "\\Unmet_" + strCat));
		vCompleted.push(getCountFromView(nv, strCat + "\\" + strCat + "\\Completed_" + strCat));
		vCancelled.push(getCountFromView(nv, strCat + "\\" + strCat + "\\Cancelled_" + strCat));		
		vAgency.push(getCountFromView(nv_1, strCat + "\\" + strCat + "\\Agency_" + strCat));
		vInHouse.push(getCountFromView(nv_1, strCat + "\\" + strCat + "\\In-House_" + strCat));
	}
	
	var vAllData = [];
	var dataSetTemplate = GetKeyword("Chart_Line_Data_Config").join(", ");
	
	Chart_DataSet(dataSetTemplate, vAllData, "Total", vTotal);
	Chart_DataSet(dataSetTemplate, vAllData, "Completed", vCompleted);
	Chart_DataSet(dataSetTemplate, vAllData, "In-House", vInHouse);
	Chart_DataSet(dataSetTemplate, vAllData, "Agency", vAgency);
	Chart_DataSet(dataSetTemplate, vAllData, "Unmet", vUnmet);
	Chart_DataSet(dataSetTemplate, vAllData, "Cancelled", vCancelled);
		
	var strJSON = '{ "labels" : [ "' + vLabels.join('", "') + '" ], ';
	strJSON += '"datasets" : [ {' + vAllData.join('}, {') + ' } ] }';
	
	return strJSON	
}

function Chart_DataSet(dataSetTemplate, vAllData, strLabel, vSeries){
	var dataSet = dataSetTemplate;
	dataSet = @ReplaceSubstring(dataSet, "@LABEL@", strLabel);
	dataSet = @ReplaceSubstring(dataSet, "@LABEL_TOTAL@", getSum(vSeries));
	dataSet = @ReplaceSubstring(dataSet, "@COLOR@", GetKeyword("Chart_Line_Color_" + strLabel));
	dataSet = @ReplaceSubstring(dataSet, "@DATA@", vSeries.join(', '));
	vAllData.push(dataSet);
}

function getSum(val){return I18n.toString(@Sum(val), "###,###")}

function getCountFromView(nv, strCat){
	var intCount = 0;
	var nvn:NotesViewNavigator = nv.createViewNavFromCategory(strCat);
	if(nvn == null){return 0}
	var nve:NotesViewEntry = nvn.getFirst();
	if(nve == null){return 0}
	var vals:java.util.Vector = nve.getColumnValues();
	return vals.elementAt(vals.size()-1);	
}

function GetRange_StartMonth(){
	if(ArchiveEnabled()){
		var nvRange:NotesView = GetDB_BOOK_Archive().getView("Chart_Range");
	} else {
		var nvRange:NotesView = GetDB_Database('BOOK').getView("Chart_Range");
	}	
	return nvRange.getColumnValues(1)[0];
}

function RCN_Progress(col)
{
//	print(col)
	if (col==null){col="2"}
	var sReportKey = document1.getItemValueString("REPORTKEY");
//	print(sReportKey);
	var ndb:NotesDatabase = GetDB_Database('RCN_UPLOAD');
	var vw:NotesView = ndb.getView("luReconProgress");
//	print(vw)
	var nav:NotesViewNavigator = vw.createViewNavFromCategory(sReportKey);
//	print(nav);
	var entry:NotesViewEntry = nav.getFirst();
//	print(entry)
	var l = [];
	var v = [];
	var total = 0;
	var ims = 0;
	var invoice = 0;
	while (entry!=null)
	{
	// 	print(entry.getColumnValues())
	//	print(entry.getColumnValues().elementAt(1).toString());
	 	l.push(entry.getColumnValues().elementAt(1).toString())
	 	v.push(@Integer(entry.getColumnValues().elementAt(@Integer(col))))
	 	total+=entry.getColumnValues().elementAt(3)*1;
	 	ims+=entry.getColumnValues().elementAt(5)*1;
	 	invoice+=entry.getColumnValues().elementAt(6)*1;
		entry = nav.getNextCategory()
	}
	var result = {data:v,label:l,total:total,ims:ims,invoice:invoice}
//	print(toJson(result));

	return toJson(result)
	
}

function RCN_ChartData(col)
{
	if (col==null){col="2"}
	var sReportKey = document1.getItemValueString("REPORTKEY");
//	print(sReportKey);
	var ndb:NotesDatabase = GetDB_Database('RCN_UPLOAD');
	var vw:NotesView = ndb.getView("luReconChartData");
//	print(vw)
	var nav:NotesViewNavigator = vw.createViewNavFromCategory(sReportKey);
//	print(nav);
	var entry:NotesViewEntry = nav.getFirst();
//	print(entry)
	var l = [];
	var v = [];

	while (entry!=null)
	{
//	 	print(entry.getColumnValues())
//		print(entry.getColumnValues().elementAt(1).toString());
	 	l.push(entry.getColumnValues().elementAt(1).toString())
	 	v.push(@Integer(entry.getColumnValues().elementAt(@Integer(col))))
		entry = nav.getNextCategory()
	}
	var result = {data:v,label:l}
//	print(toJson(result));

	return toJson(result)
	
}