function Grid_Action_MarkAsUnmet(){
	var gridID = "listxGrid";
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID) == false){return}
	ConfirmAction("This action will update the status of selected booking records to 'Unmet'", "btnMarkAsUnmet")
}
function Grid_Action_MarkAsCompleted(){
	var gridID = "listxGrid";
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID) == false){return}
	ConfirmAction("This action will update the status of selected booking records to 'Completed'", "btnMarkAsCompleted")
}
function Grid_Action_MarkAsBookingError(){
	var gridID = "listxGrid";
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID) == false){return}
	ConfirmAction("This action will update the status of selected booking records to 'Booking Error'", "btnMarkAsBookingError")
}

function Grid_Action_UpdateToBooked(){
	var gridID = "listxGrid";
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', gridID) == false){return}
	var vStatusCheck = Grid_GetSelected_Column('bookStatus', null, gridID);
	if(vStatusCheck.length == 0){return}
	if(vStatusCheck[0] != 'Altered'){Notify_Warning("Please select Bookings with the Altered Status only"); return}
	var strStatus = ''; var strCheck = '';
	for(var i=0; i<vStatusCheck.length; i++){		
		strStatus = vStatusCheck[i];		
		if(strCheck == ''){
			strCheck = strStatus
		} else {
			if(strCheck != strStatus){
				Notify_Warning("Please select Bookings with the Altered Status only"); 
				return					
			}
		}				
	}	
	ConfirmAction("This action will update the status of selected booking records to 'BOOKED'", "btnStatusToBooked");		
}

function Grid_Export_CJS(obj){
	
	Grid_Export_Excel(obj); return;
	
	// Get All IDs
	var mya = obj.getDataIDs();
	var data = obj.getRowData(mya[0]);     // Get First row to get the labels
	var colNames = new Array();  
	var ii=0; var strTemp = ""; var vCSV = []; var vRows = []; 
	
	for (var i in data){
		strTemp = GetColumn(obj, i)
		if(strTemp != "~@~HIDDEN~@~"){
			colNames[ii++] = i;		// i is variable name of column			
			vCSV.push(prepareValueForCSV(strTemp));
		}				
	}
	
	vRows.push(vCSV.join(","));
	
	//Check if any rows are selected
	var rowID = obj.jqGrid('getGridParam','selarrrow');
	if(rowID.length > 0){mya = rowID}
				
	for(i=0;i<mya.length;i++){
		data = obj.getRowData(mya[i]); // get each row
		vCSV = [];
		for(j=0;j<colNames.length;j++){		
			vCSV.push(prepareValueForCSV(data[colNames[j]]));
		}
		vRows.push(vCSV.join(","));	
	}
		
	//cLog(vRows.join("~@~"))
	var data = vRows.join('\r\n');
	saveTextAsFile($('.gbl_UserDocKey').html() + '_' + moment().format('YYMMDDHHmmss') + '.csv', data);	
}

function Grid_Export_CJS_ALL(obj){
	
	var allJQGridData = obj.jqGrid('getGridParam', 'data');
		
	var vCSV = []; var vColNames = []; var colName; var vRows = [];
	var colModel = obj.jqGrid ('getGridParam', 'colModel');
	for(var i=0; i<colModel.length; i++){
		colName = colModel[i].name;
		if(colName != "rn" && colName != "cb"){
			vCSV.push(colName);
			vColNames.push(colName)
		}	
	}
	vRows.push(vCSV.join(","));
	for(var i=0; i<allJQGridData.length; i++){
		vCSV = [];
		for(var j=0; j<vColNames.length; j++){
			vCSV.push(prepareValueForCSV(allJQGridData[i][vColNames[j]]));
		}
		vRows.push(vCSV.join(","));
	}
	
	var data = vRows.join('\r\n');
	saveTextAsFile($('.gbl_UserDocKey').html() + '_' + moment().format('YYMMDDHHmmss') + '.csv', data);
}

//This function allows us to have commas, line breaks, and double quotes in our value without breaking CSV format.
function prepareValueForCSV(val) {
	val = '' + val;
	val = val.replace(/"/g, '""');
	if(val.charAt(0) == "-"){val = val.replace("-", "")}
	return '"' + val + '"';
}
