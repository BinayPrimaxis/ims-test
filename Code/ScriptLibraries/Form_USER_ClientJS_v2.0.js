function USER_LEAV_DialogOpen(){
	
	AppForm_OnPageLoad_CJS('USER_LEAV');
	
	var dialogModal = $('.dlg_Leave');
	dialogModal.modal({backdrop: 'static', keyboard: false, show:false});		
	dialogModal.modal('show');
	dialogModal.on('hidden.bs.modal', function(e){
		$('.dlg_Leave').find('input:text').val('');
		$('.dlg_Leave').find('textarea').val('');
	});	
}

function USER_LEAV_AddUpdate(){
	
	var vData = $('.fld_USER_LEAV').val();
	var intCount = 1;
	if(vData != ''){
		vData = vData.split('~@~'); var vTemp;
		for(var i=0; i<vData.length; i++){
			vTemp = vData[i].split('|@|')[0].split('_');
			if(vTemp[0] == 'NEW'){
				if(parseInt(vTemp[1]) >= intCount){intCount++}
			}	
		}
	}
	$('.fld_USER_LEAV_DocKey').val('NEW_' + intCount.toString());
	USER_LEAV_DialogOpen();
}

function USER_LEAV_AddUpdate_OnComplete(){$('.dlg_Leave').modal('hide')}

function USER_LEAV_Edit(strDocKey){
	var strDTFrom = $('#' + strDocKey + '_dtFrom').html();	
	var strDTTill = $('#' + strDocKey + '_dtTill').html();		
	$('.fld_USER_LEAV_From_Date').val(strDTFrom.substring(0,11));
	$('.fld_USER_LEAV_Till_Date').val(strDTTill.substring(0,11));
	$('.fld_USER_LEAV_Comments').val($('#' + strDocKey + '_comments').html());
	
	if(strDTFrom.length > 12){$('.fld_USER_LEAV_From_Time').val(strDTFrom.substring(12,strDTFrom.length))}
	if(strDTTill.length > 12){$('.fld_USER_LEAV_Till_Time').val(strDTTill.substring(12,strDTTill.length))}
	
	$('.fld_USER_LEAV_DocKey').val(strDocKey);
	USER_LEAV_DialogOpen();
}

function USER_LEAV_Delete_Confirm(strDocKey){	
	ConfirmAction('This record will be deleted after you save this document.', null, "USER_LEAV_Delete('" + strDocKey + "')")	
}

function USER_LEAV_Delete(strDocKey){
	
	var strTemp = $('.fld_USER_LEAV').val();
	$('.fld_USER_LEAV').val(strTemp.replace(strDocKey, strDocKey + '~DELETE~'));
	
	strTemp = '#' + strDocKey; 
	$(strTemp + '_count').css( "text-decoration", "line-through" );
	$(strTemp + '_dtFrom').css( "text-decoration", "line-through" );		
	$(strTemp + '_dtTill').css( "text-decoration", "line-through" );
	$(strTemp + '_comments').css( "text-decoration", "line-through" );
	$(strTemp + '_actionBtn').html( "" );		
}

function USER_LEAV_AddUpdate_Validate(){
	
	var strDTFrom = strTrim($('.fld_USER_LEAV_From_Date').val());
	var strDTTill = strTrim($('.fld_USER_LEAV_Till_Date').val());
	if(strDTFrom == '' || strDTTill == ''){Notify_Warning('Please fill both From & Till Dates'); return}
	
	var dtFrom = moment(strDTFrom, 'DD MMM YYYY');
	var strTmFrom = strTrim($('.fld_USER_LEAV_From_Time').val());
	if(strTmFrom != ""){dtFrom.hour(strTmFrom.split(":")[0]); dtFrom.minute(strTmFrom.split(":")[1]);}
		
	var dtTill = moment(strDTTill, 'DD MMM YYYY'); 
	var strTmTill = strTrim($('.fld_USER_LEAV_Till_Time').val());
	if(strTmTill != ""){dtTill.hour(strTmTill.split(":")[0]); dtTill.minute(strTmTill.split(":")[1]);}
	
	var intFrom = parseFloat(dtFrom.format('YYYYMMDDHHmm')); var intTill = parseFloat(dtTill.format('YYYYMMDDHHmm'));
	if(intFrom > intTill){Notify_Warning('From Date/Time cannot be greater than Till Date/Time'); return}
	
	var strDocKey = $('.fld_USER_LEAV_DocKey').val();
		
	var vData = $('.fld_USER_LEAV').val();
	vData = vData.split('~@~'); var vTemp;
	for(var i=0; i<vData.length; i++){
		vTemp = vData[i].split('|@|');
		if(vTemp[0] != strDocKey){
			if(intFrom >= vTemp[3] && intFrom <= vTemp[6]){
				Notify_Warning('From Date/Time leave is already present in Leave Record Row #' + (i+1)); return
			}
			if(intTill >= vTemp[3] && intTill <= vTemp[6]){
				Notify_Warning('Till Date/Time leave is already present in Leave Record Row #' + (i+1)); return
			}
		}	
	}
	//Assign Full DateTime to Fields
	$('.fld_USER_LEAV_From_DateTime').val(intFrom.toString())
	$('.fld_USER_LEAV_Till_DateTime').val(intTill.toString())
	
	$('.btnLeaveAddUpdate').click();
	
}

function USER_Hours_Day_OnClick(strFldName){
	
	var fldCB = $('.fld_' + strFldName);		
	if (fldCB.is(':checked')){
		$(".fld_" + strFldName + "_Start").prop('disabled', false);
		$(".fld_" + strFldName + "_End").prop('disabled', false);				
	} else {
		$(".fld_" + strFldName + "_Start").val('');
		$(".fld_" + strFldName + "_End").val('');
		$(".fld_" + strFldName + "_Start").prop('disabled', true);
		$(".fld_" + strFldName + "_End").prop('disabled', true);
	}		
}

function handle_SkillLevel()
{
	var s = $(".SkillLevel")
	var l = $(".lang")
	console.log(l)
	var v=[];
	var c=[];
	for (var i=0;i<s.length;i++)
		{
		console.log(s[i])
		v.push(s[i].value)
		c.push(l[i].innerText + " (" + s[i].value+")")
		}
	$(".listLanguageSkill").val(v.join(","))
	$(".listLanguageConcat").val(c.join(","))
	}