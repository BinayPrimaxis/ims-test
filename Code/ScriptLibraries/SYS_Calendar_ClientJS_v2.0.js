var gbl_colorOptions = ['AliceBlue','AntiqueWhite','Aquamarine','Azure','Beige','Bisque','BurlyWood','CadetBlue','Chocolate','Coral','CornflowerBlue','DarkSalmon','DarkSeaGreen','Gainsboro','GhostWhite','Gold','GoldenRod','Khaki','Lavender','LavenderBlush','LawnGreen','LemonChiffon','LightBlue','LightCoral','LightCyan','LightGoldenRodYellow','MistyRose','Moccasin','Pink','Plum','PowderBlue'];
var gbl_CalStartDate = null;
var gbl_NotifyCal = null;
var gbl_Cal_ExistingSource = [];

function Calendar_BOOK_Open(){
	
	var strID = $('.fld_BOOK_Interpreter_DocKey').val();	
	/*var strLang = $("input[id*=BOOK_Language][class*=fld_BOOK_Language]").val();*/
	
	var objLang = $("input[id*=BOOK_Language][class*=fld_BOOK_Language]");
	if(objLang.length > 0){
		var strLang = objLang.val();		
	} else {
		var strLang = $('.div_BOOK_Language').html()
	}	
	
	var aptDt = BOOK_Booking_GetField('BOOK_ApptStartDate_Text');
		
	if(strLang == ''){Notify_Warning('Choose Language to view calendar'); return}
		
	var strURL = 'xCalendar.xsp?mode=book';
	if(strTrim(aptDt) != ''){
		strURL += '&dt=' + moment(aptDt).format('YYYYMMDD')
	}
	if(strID != ''){
		strURL += '&idk=' + strID;
	} else {
		strURL += '&idk=ALL';
	}
	if(strLang != ''){strURL += '&Lang=' + strLang}
	//cLog(strURL);
		
	$('#iframeCal').attr('src', strURL)
	$('#dlgCal').modal();
}

function Calendar_Generate(strView){
	
	//cLog("Function - Calendar_Generate is called ~~~~~~~~~~")
	
	var startDate = getQueryString('dt');
	var strMode = getQueryString('mode');
	
	if(strView == ''){strView = 'month'} //Default
	if(strMode == 'book'){strView = 'listDay'}	// if booking mode
	
	if(startDate == null){
		startDate = moment();
		if(strMode == 'book'){strView = 'basicWeek'} // if no start date from booking
	}
	
	var calObj = $('#divCalendar');
	
	calObj.fullCalendar({
		aspectRatio: 2.4,
		fixedWeekCount: false,
		defaultView: strView,
		defaultDate: startDate,
		eventLimit: true,
		eventTextColor: 'black',
		//weekNumbers: true,
		buttonText : {today: 'Today', month: 'Month', week: 'Week', day: 'Day'},
		customButtons: { 
			ListButton: {text: 'List', click: function() {Calendar_ListButton()}},
			InterpreterList: {text: 'Edit', icon:'glyphicon glyphicon-edit', click: function() {
				//This button is later replaced
			}},
			NewAndReloadBooking: {text: 'Edit', icon:'glyphicon glyphicon-plus', click: function() {
				//This button is later replaced
			}}			
    	},
		header: {left: 'prev,next today NewAndReloadBooking', center: 'title', right: 'month,basicWeek,basicDay ListButton InterpreterList'},
		eventClick: function(calEvent, jsEvent, view) {
			Notify_Calendar(calEvent, (strMode == null))
		}
		,eventRender: function(event, element, view ) {
			if(view.name.indexOf('list')>=0){
				var vDisplay = new Array;
				vDisplay.push('<b>Status</b>: ' + event.status);
				vDisplay.push('<b>Campus</b>: ' + event.site);
				//vDisplay.push('Report to: ' + event.location);
				vDisplay.push('<b>Location</b>: ' + event.clinic);
				element.find('.fc-list-item-title').append(" (" + vDisplay.join(', ') + ')'); 
			}
		}
	});

	var obj = $('.fc-icon-glyphicon');
	obj.removeClass('fc-icon'); obj.removeClass('fc-icon-glyphicon'); obj.addClass('glyphicon');
	
	var idk = getQueryString('idk');
	//This is select All Interpreter if only language is selected from Booking screen
	if(idk == "ALL"){$('.fldInterpreterList_Calendar option').prop('selected', true);}
	
	var fldIList = $('.fldInterpreterList_Calendar');
	var vAllValues = MultiSelect_GetAllOptions('fldInterpreterList_Calendar');
		
	fldIList.selectpicker({
		style: 'btn-success',
		width: 'fit',
		selectedTextFormat : 'count',
		actionsBox: (vAllValues.length > 2)			
	});
	
	fldIList.on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
		var vSelected = $(e.currentTarget).val();
		if(vSelected == null || vAllValues.length == vSelected.length){fldIList.selectpicker('toggle');}
		Calendar_AddRemoveEventSource(vSelected);
	});
	
	//Select Interpreter Button
	$('.bootstrap-select').appendTo($('.fc-right'));
	$('.fc-InterpreterList-button').remove();
	$('.bootstrap-select').find('.dropdown-menu').addClass('dropdown-menu-right')
	
	//New And Reload Booking button
	$('.btnNewAndReload').appendTo($('.fc-left'));
	$('.fc-NewAndReloadBooking-button').remove();
	
	//This will add the Event Sources to Calendar
	//cLog(fldIList.val())
	if(fldIList.val() != null){Calendar_AddRemoveEventSource(fldIList.val())}
}

function Calendar_AddRemoveEventSource(vNewSelection){
	
	//cLog('~~~~~~~~~~ Calendar_AddRemoveEventSource START ~~~~~~~~~~~~~~~~')
	var calObj = $('#divCalendar');		
	//No IDS
	if(vNewSelection == null){
		cLog('Removing All Events'); 
		calObj.fullCalendar('removeEventSources'); 
		return true;
	}
	
	var objEvents = calObj.fullCalendar('getEventSources');
	var vExistIDs = [];
	//Check if any needs to be removed
	for(var i=0; i<objEvents.length; i++){
		if(vNewSelection.indexOf(objEvents[i].id)<0){
			//cLog('Removing event for ' + objEvents[i].id);
			calObj.fullCalendar( 'removeEventSource', objEvents[i].id);
		} else {
			vExistIDs.push(objEvents[i].id);
		}
	}
	
	//cLog(vExistIDs.sort())
	
	if(vExistIDs.length > 0){		
		if(vExistIDs.sort().join('~') == vNewSelection.sort().join('~')){
			//cLog('Already present, no new id')
			return true;
		}
	}
	
	//Get events object again
	var intCurrLength = vExistIDs.length; 
	//cLog('intCurrLength - ' + intCurrLength);
	var strURL = './xJson_Data.xsp?Frm=BOOK&VK=JSON_Calendar&CalKey=';
		
	//Check if any needs to be Added
	for(var i=0; i<vNewSelection.length; i++){		
		var strID = vNewSelection[i];		
		if(vExistIDs.indexOf(strID)<0){			
			//cLog(strID + " not present.. adding to new list");
			//cLog(strURL + strID)
			calObj.fullCalendar('addEventSource',
				{id:strID, url:(strURL + strID), color: (gbl_colorOptions[intCurrLength])}
			);			
			intCurrLength++;
		}		
	}	
	//cLog('~~~~~~~~~~ Calendar_AddRemoveEventSource END ~~~~~~~~~~~~~~~~')
	return true;
}

function Notify_Calendar(calEvent, bShowLink){
	
	if(gbl_NotifyCal != null){gbl_NotifyCal.close();}
			
	var strTitle = calEvent.start.format('MMM DD, HH:mm '); 
	if(calEvent.end != null){strTitle +=  '- ' + calEvent.end.format('HH:mm') + ", "}
	strTitle += calEvent.status.toUpperCase();
	//cLog(strTitle)
	var strMessage = "Facility: " + calEvent.sitename;
	strMessage += "<br/>Service: " + calEvent.location;
	strMessage += "<br/>Ward/Clinic/Unit: " + calEvent.clinic;
	if(bShowLink){
		strMessage += "<br/><a href=javascript:gbl_NotifyCal.close();IFrame_OpenDoc('" + calEvent.id + "')>Open Booking</a>"
	}	
	//cLog(strMessage) //cLog(calEvent.source.color) //return
	gbl_NotifyCal = $.notify(
			{title: strTitle, message: strMessage},
			{type: 'minimalist', delay: 8000, newest_on_top: true, 
				template: '<div data-notify="container" style="background-color:' + calEvent.source.color +
				'" class="col-xs-10 col-sm-3 alert alert-{0}" role="alert">' + 
				'<span data-notify="title">{1}</span><span data-notify="message">{2}</span></div>'
			}
		);
}

function Calendar_ListButton(){	
	var calObj = ($('#divCalendar'));
	var viewName = calObj.fullCalendar('getView').name;
	var newView = (viewName == 'month' ? 'listMonth' : (viewName == 'basicWeek' ? 'listWeek' : 'listDay'));
	calObj.fullCalendar('changeView', newView);	
}

function MultiSelect_GetAllOptions(strClass){	
	var vAllValues = [];
	$('.' + strClass + ' option').each(function() {
		strValue = $(this).val();
		if(strValue != "ALL"){vAllValues.push(strValue)}
	});
	return vAllValues
}