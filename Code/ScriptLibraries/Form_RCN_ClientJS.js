/*jshint browser:true */
/* eslint-env browser */
/*global Uint8Array, console */
/*global XLSX */
/* exported b64it, setfmt */
/* eslint no-use-before-define:0 */
var X = XLSX;
var XW = {
	/* worker message */
	msg: 'xlsx',
	/* worker scripts */
	worker: './xlsx/xlsxworker.js'
	//worker: './xlsx/xlsx.full.min.js'
};
var vHeaderMsg = [];
var global_wb;

var process_wb = (function() {
	var OUT = $('#out');
	//var HTMLOUT = $('#htmlout');
//console.log(OUT)
	/*var get_format = (function() {
		var radios = document.getElementsByName( "format" );
		return function() {
			for(var i = 0; i<radios.length; i++) 
			{if(radios[i].checked || radios.length === 1) return radios[i].value;}
		};
	})();*/

	var to_json = function to_json(workbook) {
		var result = {};
		workbook.SheetNames.forEach(function(sheetName) {
			var roa = X.utils.sheet_to_json(workbook.Sheets[sheetName], {header:"A"});
			if(roa.length) result[sheetName] = roa;
		});
		return JSON.stringify(result, 2, 2);
	};

	/*var to_csv = function to_csv(workbook) {
		var result = [];
		workbook.SheetNames.forEach(function(sheetName) {
			var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
			if(csv.length){
				result.push("SHEET: " + sheetName);
				result.push("");
				result.push(csv);
			}
		});
		return result.join("\n");
	};*/

	/*var to_fmla = function to_fmla(workbook) {
		var result = [];
		workbook.SheetNames.forEach(function(sheetName) {
			var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
			if(formulae.length){
				result.push("SHEET: " + sheetName);
				result.push("");
				result.push(formulae.join("\n"));
			}
		});
		return result.join("\n");
	};*/

	var to_html = function to_html(workbook) {
		var htmlstr=""
		workbook.SheetNames.forEach(function(sheetName) {
			htmlstr+= X.write(workbook, {sheet:sheetName, type:'string', bookType:'html', sheetRows:3});
			
		});
		return htmlstr;
	};

	return function process_wb(wb) {
		global_wb = wb;
		var output = XSP.fromJson(to_json(wb))
		console.log(output)
		for (key in output)
			{
			var hws = XLSX.utils.json_to_sheet(output[key].slice(0,3),{sheetRows:3})
			break;
			}
		
		console.log(hws)
		var htmlOut = XLSX.utils.sheet_to_html(hws,{sheetRows:3})
		
		var firstProp;
		console.log(output)
			for(var key in output) {
				console.log("key="+key);
			    if(output.hasOwnProperty(key)) {
				        firstProp = output[key];
				        break;

			    }
			}
		console.log(firstProp)
		//if(OUT.innerText === undefined) $("#out").text(output);
		$(".out").val(XSP.toJson(firstProp));
		$(".htmlSample").val(htmlOut);
		$(".htmlSampleDisplay").html(htmlOut);
		
		checkHeaderConfig(firstProp[0])
		if(typeof console !== 'undefined') console.log("output", new Date());
	};
})();

var setfmt = window.setfmt = function setfmt() { if(global_wb) process_wb(global_wb); };

/*var b64it = window.b64it = (function() {
	var tarea = document.getElementById('b64data');
	return function b64it() {
		if(typeof console !== 'undefined') console.log("onload", new Date());
		var wb = X.read(tarea.value, {type:'base64', WTF:false});
		process_wb(wb);
	};
})();*/

var do_file = (function() {
	//var rABS = typeof FileReader !== "undefined" && (FileReader.prototype||{}).readAsBinaryString;
	//var domrabs = document.getElementsByName("userabs")[0];
	 //domrabs.disabled = !(domrabs.checked = false);
	var rABS = false;
	var use_worker = typeof Worker !== 'undefined';
	//var domwork = document.getElementsByName("useworker")[0];
	var domwork = false
//	if(!use_worker) domwork.disabled = !(domwork.checked = false);
	if(!use_worker) domwork = !(domwork = false);
	var xw = function xw(data, cb) {
		var worker = new Worker(XW.worker);
		worker.onmessage = function(e) {
			switch(e.data.t) {
				case 'ready': break;
				case 'e': console.error(e.data.d); break;
				case XW.msg: cb(JSON.parse(e.data.d)); break;
			}
		};
		worker.postMessage({d:data,b:rABS?'binary':'array'});
	};

	return function do_file(files) {
		//rABS = domrabs.checked;
		//rABS = false
		use_worker = domwork;
		var f = files[0];
		var reader = new FileReader();
		reader.onload = function(e) {
			console.log("reader on load")
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(!rABS) data = new Uint8Array(data);
			if(use_worker) xw(data, process_wb);
			else process_wb(X.read(data, {type: rABS ? 'binary' : 'array'}));
		};
		//if(rABS) reader.readAsBinaryString(f);
	
			reader.readAsArrayBuffer(f);
	};
})();

function handleDragover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.dataTransfer.dropEffect = 'copy';
}

function handleDrop(e) {
	e.stopPropagation();
	e.preventDefault();

	$($(".xlf"))[0].files = e.dataTransfer.files

	do_file(e.dataTransfer.files);
}

function fileOnChange(){
	console.log($(".xlf"))
	var files = $(".xlf")[0].files
	do_file(files)
	
}
function init_ready()
{
	//var drop = document.getElementById('drop');
	var drop = $(".drop")[0];
	console.log($(".drop"))
	console.log(drop)
	if (drop!=undefined)
		{
		if(!drop.addEventListener) return;
		drop.addEventListener('dragenter', handleDragover, false);
		drop.addEventListener('dragover', handleDragover, false);
		drop.addEventListener('drop', handleDrop, false);
		}

	
	}

function checkHeaderConfig(json)
{
	var shconfig = $(".headerConfig").val();
	console.log("headerConfig="+shconfig)
	if (shconfig=="")
		{
		$(".headerOK").val("");
		vHeaderMsg=["Validation for this agency has not been set"]
		return ""
		}
	var headc = XSP.fromJson("{"+shconfig+"}");
	console.log(headc)
	console.log(json)
	if (json == null)
		{
		var sjson = $(".out").val();
		var json_tmp = XSP.fromJson(sjson);
		var json = json_tmp[0];
	console.log(json);
		}
	var hmsg=[];

	for (var key in headc)
		{
		console.log(key)
		if (json[key])//check existance of key
			{
			//check if label is correct
			if (json[key]!=headc[key].lbl)
				{
				hmsg.push("Column " + key + " must have a label of " + headc[key].lbl)
				}
			}
		else
			{
			hmsg.push("Column " + key + " (" + headc[key].lbl + ") is missing")
			}
		
		}
	console.log(hmsg)
	if (hmsg.length==0)
		{
		$(".headerOK").val("OK");
		vHeaderMsg=[];
		
		}
	else
		{
		$(".headerOK").val("");
		vHeaderMsg=hmsg
		}

}

function getHeaderConfig()
{
	var agency = $(".fld_UP_Agency").val();
	console.log(agency)
	if (agency==null){return $(".headerConfig").val("");}
	console.log(window.location.pathname)
	var url = getNSFPath(window.location.pathname) + "/xJson_Data.xsp?RCN_Key="+agency
	console.log(url)
	 $.ajax({url: url, success: function(result){
	        $(".headerConfig").val(result);
	    }});	
}

function buildChartData()
{
	
	rcnData = XSP.fromJson($(".chartData").text())
	rcnDataColour = XSP.fromJson($(".chartColour").text())
	console.log("rcnColour="+rcnDataColour);
	Report_GetChart(rcnData,'chartRCN',rcnDataColour)
	rcnProgress = XSP.fromJson($(".progressData").text())
	rcnProgressColour = XSP.fromJson($(".chartProgressColour").text())
	Report_GetChart(rcnProgress,'chartProgress',rcnProgressColour)
	console.log(rcnProgress)
	$(".dblAccepted").text(formatMoney(rcnProgress.total));
	$(".dblIMS").text(formatMoney(rcnProgress.ims));
	$(".dblInvoice").text(formatMoney(rcnProgress.invoice));
	}

function getNSFPath(sURL) {
	// gets the url up to and including the .nsf
	var iIndex;
	iIndex = sURL.indexOf('.nsf');
	if (iIndex <= 0)
		iIndex = sURL.indexOf('.ntf');
	if (iIndex > 0)
		sURL = sURL.substr(sURL, iIndex + 4);

	return sURL;
}

function updateRCNData()
{
	rcnData = XSP.fromJson($(".chartData").text())
	rcnDataColour = XSP.fromJson($(".chartColour").text())
	reportUpdate(rcnData,'chartRCN',rcnDataColour)
	}

function updateRCNProgress()
{
	rcnData = XSP.fromJson($(".progressData").text());
	rcnProgressColour = XSP.fromJson($(".chartProgressColour").text());
	reportUpdate(rcnData,'chartProgress',rcnProgressColour);
	$(".dblAccepted").text(formatMoney(rcnData.total));
	$(".dblIMS").text(formatMoney(rcnProgress.ims));
	$(".dblInvoice").text(formatMoney(rcnProgress.invoice));
	}
function htmlTable(json)
{
	html=""
	for (key in json)
		{
		
		}
	
	}

function formatMoney(n, c, d, t) {
	  var c = isNaN(c = Math.abs(c)) ? 2 : c,
	    d = d == undefined ? "." : d,
	    t = t == undefined ? "," : t,
	    s = n < 0 ? "-" : "",
	    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
	    j = (j = i.length) > 3 ? j % 3 : 0;

	  return "$" + s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}
