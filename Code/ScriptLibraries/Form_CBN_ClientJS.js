function Grid_CBN_Create(){
	if(Grid_GetSelected_Column('@unid', 'selUNIDs', "listxGrid") == false){return}
	ConfirmAction("This action will combine bookings. ", "btnCBN_Create")
}

function CBN_ChangeInterpreterHoursUpdate_Validate(){	
	
	var strBookDt = BOOK_Booking_GetField('CBN_ApptStartDate_Text');
	if(strBookDt.length != 11){ChangeInterpreterHours_ErrorControl('Booking Date is not set properly'); return}
	
	var dtBookStart = moment(strBookDt,'DD MMM YYYY', true);
	if(dtBookStart.isValid() == false){ChangeInterpreterHours_ErrorControl('Date not valid as per moment class'); return}
	
	var strTime = CBN_GetTimeText();
	console.log("strTime="+strTime);
	if(strTime.length != 5 || strTime.indexOf(':') != 2){ChangeInterpreterHours_ErrorControl('Booking Time is not set properly'); return}
	
	var strDuration = CBN_GetDurationText();
	if(strDuration.length == 0){ChangeInterpreterHours_ErrorControl('Booking Duration is not entered'); return}
	
	if(ChangeInterpreterHours_Validate(false, "", strTime, strDuration)){
		$('#dlg_ChangeInterpreterHours').modal('hide');
		$('.btnCBNChangeInterpreterHoursUpdate_SJS').click();
	};	
}

function CBN_GetTimeText(){
	var strReturn = strTrim($('.div_CBN_ApptStartTime_Text').html());
	console.log(strReturn);
	if(strReturn.indexOf('<span')>=0){strReturn = strReturn.split('<span')[0]}
	
	
	return strReturn
}

function CBN_GetDurationText(){
	var strReturn = strTrim($('.div_CBN_ApptStartTime_Text').html());
	console.log(strReturn);
	if(strReturn.indexOf('(')>=0){
		strReturn = strReturn.split('(')[0]
		strReturn = strReturn.split(' min')[0]
		
	}
	
	
	return strReturn
}

function CBN_Save_OnComplete(){	
	console.log("CBN_Save_OnComplete")
	var strMsg = $('.BOOK_CheckAvailability').html();
	cLog(strMsg)
	if(strMsg == ''){
		SaveNotification_OnComplete(); 
		UpdateFTIndex('xFTIndex_Agent.xsp');
		var gridObj = parent.$('#listxGrid');
		if(gridObj.length > 0){parent.Grid_Generate_DateRange()}
		return
	}
	
	SaveNotification_Close();
	
	var bCheck1 = ($('.scInterperterOnly').html() == 'N');			//Coordinator or Manager or DBAdmin
	var bCheck2 = ($('#dlg_ChangeInterpreterHours').length > 0);	//Planner is enabled
	var bCheck3 = ($('.scDatePassed').html() == ''); 				//Booking to be of future date
	var bCheck4 = (strMsg.toLowerCase().indexOf('leave') < 0);		//No Leave on that day for Interpreter
	
	cLog(strMsg); //MeaningFull Message ~ strIName ~ Code ~ BookingDate ~ DateTimes
	var vMsg = strMsg.split('~');
	cLog(bCheck1)
	cLog(bCheck2)
	cLog(bCheck3)
	cLog(bCheck4)
	if(bCheck1 && bCheck2 && bCheck3 && bCheck4){			
		
		strMsg += '.<br/>Do you wish to modify working hours for this booking day ?';		
		$('#dlg_ChangeInterpreterHours_Date').html(BOOK_Booking_GetField('CBN_ApptStartDate_Text'));
		$('#dlg_ChangeInterpreterHours_Msg').html(vMsg[0]);	//MeaningFull Message
		$('#dlg_ChangeInterpreterHours').modal();
		
		//if Length is greater than 5 it means Interpreter has some hours set for this day
		//Set Hours coming in with message
		console.log("interpretor hours change");
		console.log(vMsg)
		if(vMsg.length > 5){			
			$('.fld_NewTime_Start_1').val(vMsg[4]);
			$('.fld_NewTime_End_1').val(vMsg[5]);
			//Split hours present check
			$('.fld_NewTime_Start_2').val(((vMsg.length > 6) ? vMsg[6] : ''));
			$('.fld_NewTime_End_2').val(((vMsg.length > 6) ? vMsg[7] : ''));
			$('.fld_SplitHours').prop("checked", vMsg.length > 6);			
		}
		
		JQ_ShowHide('divTime_2nd', vMsg.length > 6);
		
	} else {
		$('.BOOK_CheckAvailability').html("");
		Dialog_Message("Interpreter not available", vMsg[0]);	
		
	}

}

function Grid_CBN_AddBooking(){
	var gridID = "listx_GRID_CBN_BOOK_Search";
	var fldClass = "selUNIDs_GRID_CBN_BOOK_Search"
	if(Grid_GetSelected_Column('@unid', fldClass, gridID) == false){return}
	cLog($('.' + fldClass).val().indexOf('~'))
	if($('.' + fldClass).val().indexOf('~')>0){Notify_Warning("Please select one booking at a time");return false}
	ConfirmAction('Are you sure you wish to combine selected bookings from suggestion to this combine booking document', 'btnCBN_BOOK_Add_SJS')
}
function Grid_CBN_AddBooking_OnComplete(){
	//Called from ServereJS
	UpdateFTIndex('xFTIndex_Agent.xsp');
	var gridObj = parent.$('#listxGrid');
	if(gridObj.length > 0){parent.Grid_Generate_DateRange()}
	$('.btnCBN_Reload').click();
}