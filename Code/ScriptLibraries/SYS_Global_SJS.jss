import SYS_Charts_SJS; import SYS_Grid_SJS; import SYS_LDAP_SJS; import SYS_TypeAhead_SJS; import Form_BOOK_SJS; 
import SYS_Grid_BOOK_Actions_SJS; import Form_CBN_SJS; import Form_RCN_SJS;

if (!Array.prototype.indexOf) {	
	Array.prototype.indexOf = function(obj, start) {
	     for (var i = (start || 0), j = this.length; i < j; i++) {
	         if (this[i] === obj) { return i; }
	     }
	     return -1;
	}	
}

function GetHomePage(){
	
	var strHomePage = '';	
	if(sessionScope.containsKey('ssHome_Default') == false){
		if(IsUser_Manager()){
			strHomePage = 'Manager';
		} else if (IsUser_Coordinator() | IsUser_DBAdmin()){
			strHomePage = 'Coordinator';
		} else if (IsUser_Interpreter()){
			strHomePage = 'Interpreter';
		} else if (IsUser_Requestor()){
			strHomePage = 'Requestor';
		}	
		sessionScope.put('ssHome_Default', strHomePage);
	}
	
	strHomePage = sessionScope.ssHome_Default;
	//Check for switching exists or not
	if(sessionScope.containsKey('ssHome_Switch')){
		if(sessionScope.ssHome_Switch == strHomePage){
			sessionScope.remove('ssHome_Switch');
		} else {
			strHomePage = sessionScope.ssHome_Switch;
		}	
	}	
	
	if (strHomePage=="Manager"){
		var dash = GetKeyword("Dashboard_Link");
		if (dash!="" && dash!=null){
			strHomePage = "Dashboard"
		}
	}
	
	sessionScope.put('ssHome_Current', strHomePage);
	return '/ccApp_Home_' + strHomePage + '.xsp';
}

function GetFormName(){
	if(viewScope.containsKey('gbl_Form') == false){
		if(param.KEY == null){viewScope.put('gbl_Form', 'BOOK')} else {viewScope.put('gbl_Form', param.KEY)}
	}
	return viewScope.gbl_Form
}

function GetCustomControl(){return '/ccForm_' + GetFormName() + '.xsp'}

function GetFormProperty(strForm, strPropName){
	
	if(applicationScope.containsKey(strForm + '_Config') == false){
		
		var strHeading = GetKeyword(strForm + '_Heading');
		if(strHeading == ""){
			Global_LogError('~~ ERROR in GetFormProperty : ' + strForm + '_Heading - Keyword is not available.')
			return null
		}
		
		var strFilePath = GetKeyword('DB_FilePath_' + strForm)
		if(strFilePath == ''){
			strFilePath = database.getFilePath(); //That means using current database
		}
		
		var vHM:java.util.HashMap = new java.util.HashMap(); 
		vHM.put('formHeading', strHeading);
		vHM.put('DB_FilePath', strFilePath);
				
		applicationScope.put(strForm + '_Config', vHM);		
	}
	
	return applicationScope.get(strForm + '_Config').get(strPropName)		
}

function App_BeforePageLoad(){
	try{
	var pageName = view.getPageName();
	
	if(pageName == '/xREQU.xsp' | pageName == '/xNoAccess.xsp' | pageName == '/xLogOut.xsp'){return}
	
	if(IsUser_Requestor()){
		if(pageName == '/xForm.xsp' & param.KEY == 'REQU' & param.documentId == General_GetUser_UNID()){return}
	}
	
	if(!(IsUser_Manager() | IsUser_DBAdmin())){		
		if(IsUser_Coordinator() | IsUser_Interpreter()){
			if(pageName == '/xCalendar.xsp' & param.mode == 'book'){return}
		}
		if(IsUser_Coordinator() & GetKeyword('Planner_Coordinator_Access') != 'NO'){
			if(pageName == '/xPlanner.xsp'){return}
		}
		if(IsUser_Coordinator()){
			if(pageName == '/xRCN.xsp'){return}
			if(pageName == '/xView.xsp' & param.KEY=="RCN_Upload"){return}
			if(pageName == '/xView.xsp' & param.KEY=="CST"){return}
			if(pageName == '/xForm.xsp' & param.KEY=="CST"){return}
		}
		if(@IsMember(pageName, ['/xReports.xsp', '/xPlanner.xsp', '/xView.xsp', '/xCalendar.xsp', '/xForm.xsp'])){			
			OpenLog_Event(General_GetUser_FullName() + ' Access Denied to Page - ' + pageName)			
			context.redirectToPage('xNoAccess.xsp');
		}	
	}	
	}
	catch(err)
	{
		print(err.toString())
	}
}

function AppForm_BeforePageLoad(docX:NotesXspDocument){	
	try{
	var strForm = docX.getForm();	
	if(docX.isNewNote()){		
		docX.setValue('DocKey', AppForm_DocKey(strForm));
		docX.setValue("DocKey_CreatedBy", General_GetUser_DocKey());
		docX.setValue("CreatedDate", session.createDateTime(@Today()).getDateOnly());
		docX.setValue("CreatedBy", General_GetUser_FullName());		
	} else {		
		docX.setValue("ModifiedDate", session.createDateTime(@Today()).getDateOnly());
		docX.setValue("ModifiedBy", General_GetUser_FullName());		
	}	
	}
	catch(err)
	{
		Global_LogError(err.toString(),docX)
	}
}

function AppLogo(){
	if (applicationScope.containsKey('logoFilePath') == false){applicationScope.put('logoFilePath', '/CompanyLogo/0/$FILE/' + GetKeyword('Global_Logo'))}
	return applicationScope.get('logoFilePath');
}

function IsPMS(){
	if(applicationScope.containsKey('asIsPMS') == false){
		applicationScope.put('asIsPMS', (GetKeyword('Global_PMS_Integration') == 'YES'))
	}
	return applicationScope.get('asIsPMS');
}

function IsAuth_Domino(){	
	if(sessionScope.containsKey('ssAuthType') == false){
		sessionScope.put('ssAuthType', (GetKeyword('Global_AuthenticationType') == 'Domino'))
	}
	return sessionScope.get('ssAuthType');
}

function USER_GetProfileDocument(strUserName){return GetDB_Database("USER").getView("USER_ByUserName").getDocumentByKey(strUserName, true)}

function REQU_GetProfileDocument(strUserName){
	var dc:NotesDocumentCollection = GetDB_Database("REQU").search('Form = "REQU" & REQU_EffectiveUserName = "' + strUserName + '"', null, 0);
	if(dc.getCount() == 0){return null}
	return dc.getFirstDocument();
	//return GetDB_Database("REQU").getView("REQU_ByUserName").getDocumentByKey(strUserName, true)
}

function JSON_Data(){
	
	//Charts Call
	if(param.ChartKey != null){Chart_JSON(); return}
	
	//TypeAhead Call
	if(param.TAType != null){TypeAhead_JSON(); return}
	
	//TypeAhead Call
	if(param.RCN_Key != null){RCN_Upload_Config_JSON(); return}
	
	//Else rest are managed by this function, Grid & Calendar
	Grid_JSON_Java();	
}


function User_ResetSessionScopes(){
	var strUserName = session.getEffectiveUserName();
	var nn:NotesName = session.createName(strUserName);
	//print(nn.getCommon() + "~~~~~~");
	var docUser:NotesDocument = USER_GetProfileDocument(strUserName);
	var rolesACL:java.util.Vector = database.queryAccessRoles(strUserName);	
	var strUNID = ''; var strFullName; var vRoles = []; var vLang = []; 
	var strDocKey; var strPhone = ''; var bIsDBAdmin = false; var vLocation = []; var strSiteCode = '';
	
	if(docUser == null){
		
		if(rolesACL.size() == 0){
			
			//Check if user is requestor
			var docREQU:NotesDocument = REQU_GetProfileDocument(strUserName);
			if(docREQU != null){				
				
				vRoles.push('Requestor');
				strUNID = docREQU.getUniversalID();
				strFullName = docREQU.getItemValueString("REQU_FullName");
				strDocKey = docREQU.getItemValueString("DocKey");
				strPhone = docREQU.getItemValueString('REQU_Phone');
				vLocation = docREQU.getItemValue('REQU_Location');
				strSiteCode = docREQU.getItemValueString('REQU_Site');//Updated by Baljit - Task - Requestor Login
			} else {
				//print('IMS~DeBug~User not found: ' + strUserName);
				if(GetKeyword("Global_SelfRegister") == "YES"){
					print('redirecting to self register ~~~~~~~~~~~');
					context.redirectToPage('xREQU.xsp?KEY=REQU');
				} else {
					context.redirectToPage('xNoAccess.xsp?key=NU')
				}
			}
		}
		
		if(rolesACL.indexOf("[PrimaxisAppMgr]")>=0){
			//Assign All other Roles to Primaxis Application Manager for Home page purpose
			strFullName = nn.getCommon();
			strDocKey = nn.getCommon();
			bIsDBAdmin = true;
		}
		
	} else {
		
		vRoles = docUser.getItemValue("USER_Role_Secondary");
		if(IsNullorBlank(vRoles)){
			vRoles = docUser.getItemValue("USER_Role_Primary");
		} else {
			vRoles.push(docUser.getItemValueString("USER_Role_Primary"))
		}
		strUNID = docUser.getUniversalID();
		strFullName = docUser.getItemValueString("USER_FullName");
		vLang = docUser.getItemValue("USER_Languages");
		strDocKey = docUser.getItemValueString("DocKey");
		strPhone = docUser.getItemValueString('USER_Phone');
		
		var strDefRole = docUser.getItemValueString('USER_Role_Default');
		if(!IsNullorBlank(strDefRole)){sessionScope.put('ssHome_Default', strDefRole)}
	}
	
	sessionScope.put("ssUser_DocKey", strDocKey);
	sessionScope.put("ssUser_UNID", strUNID);
	sessionScope.put("ssUser_FullName", strFullName);
	sessionScope.put("ssUser_IsDBAdmin", bIsDBAdmin);
	sessionScope.put("ssUser_IsManager", @IsMember("Manager", vRoles));
	sessionScope.put("ssUser_IsCoordinator", @IsMember("Coordinator", vRoles));
	sessionScope.put("ssUser_IsInterpreterAlso", @IsMember("Interpreter", vRoles));
	var bInterpreter = @IsMember("Interpreter", vRoles) & !(sessionScope.ssUser_IsCoordinator | sessionScope.ssUser_IsManager | sessionScope.ssUser_IsDBAdmin)
	sessionScope.put("ssUser_IsInterpreter", bInterpreter);
	sessionScope.put("ssUser_IsRequestor", @IsMember("Requestor", vRoles));
	sessionScope.put("ssUser_LanguageList", vLang);	
	sessionScope.put("ssUser_Phone", strPhone);
	sessionScope.put("ssUser_Location", vLocation);
	sessionScope.put("ssUser_SiteCode", strSiteCode);
	sessionScope.put("ssUser_SiteName", strSiteCode);//Updated by Baljit - Task - Requestor Login
}

function ssGetVariable(strVar){
	if(sessionScope.containsKey(strVar) == false){
		//print('Calling Reset Session Scope - ' + strVar)
		User_ResetSessionScopes()
	}
	return sessionScope.get(strVar)
}
function General_GetUser_UNID(){return ssGetVariable("ssUser_UNID")}
function General_GetUser_DocKey(){return ssGetVariable("ssUser_DocKey")}
function General_GetUser_FullName(){return ssGetVariable("ssUser_FullName")}
function IsUser_DBAdmin(){return ssGetVariable("ssUser_IsDBAdmin")}
function IsUser_Manager(){return ssGetVariable("ssUser_IsManager")}
function IsUser_Coordinator(){return ssGetVariable("ssUser_IsCoordinator")}
function IsUser_Interpreter(){return ssGetVariable("ssUser_IsInterpreter")}
function IsUser_Requestor(){return ssGetVariable("ssUser_IsRequestor")}

function CGIVariables() {
	
	/* 
		This server-side JavaScript library implements a class named "CGIVariables" which allows for easy access 
		to most CGI variables in XPages via JavaScript. 
		For example, to dump the remote users name, IP address and browser string to the server console, use: 
	*/ 

	try {
		var vList = [];
		var objReq = facesContext.getExternalContext().getRequest(); 
		var objSer = facesContext.getExternalContext().getContext();
	} catch(e) { 
		print (e.message);
		return null;
	}
	      
	vList.push(['CGI_AUTH_TYPE', 				objReq.getAuthType()]);
	vList.push(['CGI_CONTENT_LENGTH', 			objReq.getContentLength()]);
	vList.push(['CGI_CONTENT_TYPE', 			objReq.getContentType()]);
	vList.push(['CGI_CONTEXT_PATH', 			objReq.getContextPath()]);
	vList.push(['CGI_HTTPS', 					objReq.isSecure() ? 'ON' : 'OFF']);
	vList.push(['CGI_PATH_INFO', 				objReq.getPathInfo()]);
	vList.push(['CGI_PATH_TRANSLATED', 			objReq.getPathTranslated()]);
	vList.push(['CGI_QUERY_STRING', 			objReq.getQueryString()]);
	vList.push(['CGI_REMOTE_ADDR', 				objReq.getRemoteAddr()]);
	vList.push(['CGI_REMOTE_HOST', 				objReq.getRemoteHost()]); 
    vList.push(['CGI_REMOTE_USER', 				objReq.getRemoteUser()]);
    vList.push(['CGI_REQUEST_METHOD', 			objReq.getMethod()]);
    vList.push(['CGI_REQUEST_SCHEME', 			objReq.getScheme()]);
    vList.push(['CGI_REQUEST_URI', 				objReq.getRequestURI()]); 
    vList.push(['CGI_SCRIPT_NAME ', 			objReq.getServletPath()]); 
    vList.push(['CGI_SERVER_NAME', 				objReq.getServerName()]);
    vList.push(['CGI_SERVER_PORT', 				objReq.getServerPort()]);
    vList.push(['CGI_SERVER_PROTOCOL', 			objReq.getProtocol()]);
    vList.push(['CGI_SERVER_SOFTWARE', 			objSer.getServerInfo()]); 

	// these are not really CGI variables, but useful, so lets just add them for convenience 
	vList.push(['CGI_HTTP_ACCEPT', 				objReq.getHeader("Accept")]);
	vList.push(['CGI_HTTP_ACCEPT_ENCODING', 	objReq.getHeader("Accept-Encoding")]); 
	vList.push(['CGI_HTTP_ACCEPT_LANGUAGE', 	objReq.getHeader("Accept-Language")]);
	vList.push(['CGI_HTTP_CONNECTION', 			objReq.getHeader("Connection")]);
	vList.push(['CGI_HTTP_COOKIE', 				objReq.getHeader("Cookie")]);
	vList.push(['CGI_HTTP_HOST', 				objReq.getHeader("Host")]);
	vList.push(['CGI_HTTP_REFERER', 			objReq.getHeader("Referer")]);
	vList.push(['CGI_HTTP_USER_AGENT', 			objReq.getHeader("User-Agent")]);
	
	var xspUA:XSPUserAgent = context.getUserAgent();
	vList.push(['CGI_BROWSER', 					xspUA.getBrowser() + "_" + xspUA.getBrowserVersionNumber()]);
	
	return vList;
}

function Global_LogError(strErrMsg, document, logDoc){	
	view.postScript("Notify_Warning('" + strErrMsg + "')");
	OpenLog_Error(strErrMsg, document, logDoc);
	print(@DbTitle() + ': ' + strErrMsg)
}

function Global_Notify(strErrMsg){view.postScript("Notify_Warning('" + strErrMsg + "')")}

function OpenLog_CreateDocument(msg, document){
	
	var logdb:NotesDatabase = session.getDatabase(database.getServer(), GetKeyword("DB_FilePath_OpenLog"));
	var doc:NotesDocument = logdb.createDocument();
	var nnUser:NotesName = session.createName(session.getEffectiveUserName());
	doc.appendItemValue("Form", "LogEvent");
	doc.appendItemValue("LogUserName", nnUser.getCommon());
	doc.appendItemValue("LogEffectiveName", nnUser.getAbbreviated());
	doc.appendItemValue("LogClientVersion", session.getNotesVersion());
	doc.appendItemValue("LogFromServer", database.getServer());
	doc.appendItemValue("LogFromDatabase", database.getFilePath());
	doc.appendItemValue("LogEventTime", session.createDateTime(new Date()));
	doc.appendItemValue("LogAgentStartTime", session.createDateTime(new Date()));
	doc.appendItemValue("LogStackTrace", context.getUrl().getQueryString());
	doc.appendItemValue("LogFromAgent", context.getUrl().getAddress());
	doc.appendItemValue("LogMessage", msg);	
	if (document){doc.createRichTextItem("LogDocInfo").appendDocLink(document)}
	return doc;
}

function OpenLog_Error(msg, document, logDoc){
	try {
		if(!logDoc){
			var logDoc:NotesDocument = OpenLog_CreateDocument(msg, document);
			logDoc.appendItemValue("LogEventType", "Error");
			logDoc.appendItemValue("LogErrorMessage", msg);
			logDoc.appendItemValue("LogSeverity", "2");
		} else {
			logDoc.replaceItemValue("LogMessage", logDoc.getFirstItem("LogMessage").getText() + '\r' + msg);
			logDoc.replaceItemValue("LogErrorMessage", logDoc.getFirstItem("LogErrorMessage").getText() + '\r' + msg);
		}				
		logDoc.save();
		return logDoc;
	}catch (e){
		print("ERROR in OpenLog_Error while trying to log error: " + msg);
	}
}

function OpenLog_Event(msg, document, logDoc){
	try {
		if(!logDoc){
			var logDoc:NotesDocument = OpenLog_CreateDocument(msg, document);
			logDoc.appendItemValue("LogEventType", "Event");
			logDoc.appendItemValue("LogSeverity", "0");
		} else {
			logDoc.replaceItemValue("LogMessage", logDoc.getFirstItem("LogMessage").getText() + '\r\r' + msg);
		}		
		logDoc.save();
		return logDoc;
	}catch (e){
		print( "ERROR: Error in OpenLog_Event while trying to log event: " + msg);
	}
}

function Previous_Login_DateTime(bReturnValue){
	
	var logdb:NotesDatabase = session.getDatabase(database.getServer(), GetKeyword("DB_FilePath_OpenLog"));
	
	//Create Login Document
	if(!sessionScope.bLoginCreated | sessionScope.bLoginCreated == null){
		
		var vTemp = facesContext.getExternalContext().getRequest().getHeader("Cookie")
		var strKey = 'No COOKIE - Session Authentication not enabled for Domino'
		if(vTemp != null){
			vTemp = vTemp.split(';')		
			strKey = strUser + "_" + @Right(vTemp[1], "=") + "_" + @Right(vTemp[0], "=")	
		}
		
		var strUser = session.getEffectiveUserName();			
		var logvw:NotesView = logdb.getView("lkp-loginevents");
		var logDoc:NotesDocument = logvw.getDocumentByKey(strKey, true);
			
		//Create this login document
		if (logDoc == null){
			logDoc = OpenLog_CreateDocument(strKey);
			logDoc.replaceItemValue("LogEventType", "LOGIN");
			//CGI Variables
			var vList = CGIVariables();			
			if(vList != null){		
				for(var i=0; i<vList.length; i++){
					logDoc.replaceItemValue(vList[i][0], vList[i][1]);
					if(vList[i][0] == 'CGI_HTTP_HOST'){logDoc.replaceItemValue("Host", vList[i][1])}
					if(vList[i][0] == 'CGI_BROWSER'){logDoc.replaceItemValue("InternetBrowser", vList[i][1])}
				}		
			}			
			//Save Document
			logDoc.computeWithForm(true, false);
			logDoc.save(true, false);		
			logDoc.recycle();
		} 			
		sessionScope.put("bLoginCreated", true)		
	}
	
	if(!bReturnValue){return}
	
	//Last Login Datetime
	if(!sessionScope.strLastLogin | sessionScope.strLastLogin == null){
		var logvw:NotesView = logdb.getView("lkp-lastlogin")
		var lognvec:NotesViewEntryCollection = logvw.getAllEntriesByKey(session.getEffectiveUserName(), true)
		if (lognvec.getCount() < 2){
			sessionScope.put("strLastLogin", "No information/First time login")
		} else {
			var lognve:NotesViewEntry = lognvec.getFirstEntry()
			lognve = lognvec.getNextEntry(lognve)
			sessionScope.put("strLastLogin", lognve.getColumnValues()[2])
		}
	}
	
	return sessionScope.strLastLogin		
}

function AppLogOut(){
	var iterator = sessionScope.keySet().iterator();
	while( iterator.hasNext() ){sessionScope.remove( iterator.next() )} // Remove all items
	//facesContext.getExternalContext().redirect("./?logout&redirectto=" + facesContext.getExternalContext().getRequestContextPath() + '/xLogOut.xsp');
	//facesContext.getExternalContext().redirect("./?logout&redirectto=/domcfg.nsf/Logout");
	facesContext.getExternalContext().redirect("./?logout&redirectto=" + facesContext.getExternalContext().getRequestContextPath());
}

function GetValidationJSON(strKeyName:String, strExtraKey){	
	var strKey = strKeyName + '_ValidationJSON'
	if(strExtraKey){strKey += '_' + strExtraKey}
	var vList = GetKeyword(strKey);	
	if(isString(vList) == false){vList = vList.join(',')}
	if(vList != ''){vList = '[' + vList + ']'}	
	return vList;
}

function GetKeyword_PublicAccess(strKeyName:String){
	var nv:NotesView = sessionAsSigner.getCurrentDatabase().getView('KWD_byName');
	var doc:NotesDocument = nv.getDocumentByKey(strKeyName, true);
	if(doc != null){
		if(doc.getItemValueString("KWType") == 'File Attachment'){
			return doc.getItemValueString('KWFileName')
		} else if(doc.getItemValueString("KWType") == 'Text'){
			return doc.getItemValueString('KWText')
		} else {
			return doc.getItemValue('KWMultiText')
		}
	}	
}

function GetKeyword(strKeyName:String){return @DbLookup("", "kwd_byName", strKeyName, 2, "[FAILSILENT]")}
function GetAliasedKeyword (strKeyName:String){
	//return whats left of | from a keyword
	var vKeyList = GetKeyword (strKeyName);
	var vRetList = [];
	
	for(var i=0; i < vKeyList.length; i++){
		if (@Contains (vKeyList[i], "|"))
		{
			vRetList.push (@Left (vKeyList[i], "|"));
		}else{
			vRetList.push (vKeyList[i]);
		}
	}
	return vRetList;
}

function getColumnList(viewName:String,col)
{
	if (col==null){col=1}
	var list = @DbColumn("",viewName,col);
	if (list.constructor!=Array)
	{
		return [list]
	}
	else
	{
		return list
	}
}

function KeywordLabelValue(strValue, strKey){		
	var vKeyList = GetKeyword(strKey);	
	for(var i=0; i < vKeyList.length; i++){if(@Right(vKeyList[i],"|") == strValue){return @Left(vKeyList[i],"|")}}
	return strValue
}
function KeywordValue_FromLabel(strValue, strKey){		
	var vKeyList = GetKeyword(strKey);	
	for(var i=0; i < vKeyList.length; i++){if(@Left(vKeyList[i],"|") == strValue){return @Right(vKeyList[i],"|")}}
	return null
}

function GetDateFormat(){return GetKeyword('Global_DateFormat')[0]}
function GetDateControl_Options(){return GetKeyword('Global_DateFormat')[1]}
function GetTimeFormat(){return GetKeyword('Global_TimeFormat')[0]}
function GetTimeControl_Options(){return GetKeyword('Global_TimeFormat')[1]}
function GetDateTimeFormat(){return GetKeyword('Global_DateTime_Format')}

function FormatDateTime(strDateTimeString, strType){
	if(strType == 'Date'){return I18n.toString(@Date(strDateTimeString), GetDateFormat())}
	if(strType == 'Time'){return I18n.toString(@Time(strDateTimeString), GetTimeFormat())}
	return strDateTimeString
}

function DIV_AppForm_ChildDoc_StyleClass(bRight){
	var bsColumn = 3;	
	if(compositeData.bsColumn){bsColumn = compositeData.bsColumn}
	if(bRight){bsColumn = 12-bsColumn; if(bsColumn == 0){bsColumn = 12}}	
	return 'col-lg-' + bsColumn + ' col-md-' + bsColumn + ' col-sm-12 col-xs-12';
}

function DIV_FieldLabel_StyleClass(bsColumn){
	if (bsColumn==null){ bsColumn=4;} 
	if (compositeData.bsColumn){bsColumn=compositeData.bsColumn}		
	return 'col-lg-' + bsColumn + ' col-md-' + bsColumn + ' col-sm-12 col-xs-12';	
}

function DIV_Field_StyleClass(bsColumn){
	var strClass = 'div_' + compositeData.field_Name;	
	if (bsColumn==null){ bsColumn=8;} 
	if (compositeData.bsColumn){bsColumn = 12-compositeData.bsColumn;}	
	return 'col-lg-' + bsColumn + ' col-md-' + bsColumn + ' col-sm-12 col-xs-12 ' + strClass;	
}

function AppForm_DocKey(strForm:String) {
	
	//strUser is optional, and defaults to the current user if not passed.
	var strName:String = General_GetUser_FullName();
	var strDocKey:String = strForm + "-";
	if (strForm=="BOOK"){strDocKey="";}
	var str4:String;
	var strUniq:String = session.evaluate("@Unique").join("");

	if(strName.indexOf(" ") >= 0){
		if(strName.lastIndexOf(" ")<strName.length-2)
			str4 = strName.charAt(0)
				+ strName.substr(strName.lastIndexOf(" ") + 1, 2)
				+ strName.charAt(strName.length-1);
		else
			str4 = strName.charAt(0)
				+ strName.substr(strName.lastIndexOf(" ") + 1);
	}else{
		if(strName.length>3)
			str4 = strName.substr(0, 3)
				+ strName.charAt(strName.length-1);
		else
			str4 = strName; 
	}
	if(str4.length < 4)
		str4 = str4 + "ZZZZ".substr(0,4-str4.length);
		
	strDocKey = strDocKey + str4.toUpperCase() + "-";
	strDocKey = strDocKey + strUniq.substring(strUniq.indexOf("-") + 1);
	
	return strDocKey; 
}

function Global_RemoveDocument(nDoc){
	try{

	var strServer = database.getServer();
	var delDB:NotesDatabase = sessionAsSigner.getDatabase(strServer, GetKeyword('DB_Deletions'));
	var delDoc:NotesDocument = sessionAsSigner.getDatabase(strServer, nDoc.getParentDatabase().getFilePath()).getDocumentByID(nDoc.getNoteID());
	var newDoc:NotesDocument = delDoc.copyToDatabase(delDB);
	delDoc.remove(true);
	newDoc.replaceItemValue('DeletedBy_DocKey', General_GetUser_DocKey());
	newDoc.replaceItemValue('DeletedBy', General_GetUser_FullName());
	newDoc.save(true, false);
	}
	catch(err)
	{
		OpenLog_CreateDocument(err.toString())
	}
}

function GetDB_FilePath(strForm){
	if(strForm == 'CBN'){strForm = 'BOOK'}
	return GetFormProperty(strForm, 'DB_FilePath')
}
function GetDB_Database(strForm){
	if(strForm == 'CBN'){strForm = 'BOOK'}
	return session.getDatabase(database.getServer(), GetDB_FilePath(strForm), false)
}
function GetDocByDocKey(skey,strForm){
	if (strForm==null){strForm="BOOK"}
	var dbDocKey:NotesDatabase = GetDB_Database(strForm);
	var vwDocKey = dbDocKey.getView("vwAllByDocKey");
	return vwDocKey.getDocumentByKey(skey,true);
}
function GetDB_NAB(){return sessionAsSigner.getDatabase(database.getServer(), GetKeyword("DB_FilePath_NAB"), false)}
function GetDB_BOOK_Archive(){return session.getDatabase(database.getServer(), GetKeyword('DB_FilePath_BOOK_Archive'), false)}
function ArchiveEnabled(){
	if(sessionScope.containsKey('archiveEnabled') == false){sessionScope.put('archiveEnabled', GetKeyword('Global_BOOK_Archive_Enabled') == "YES")}
	return sessionScope.get('archiveEnabled')
}
function GetValue(strField){return @Trim(docX.getItemValueString(strField))}
function isArray(ele){if(typeof ele == "java.util.Vector"){return true} else {return false}}
function isString(ele){if(typeof ele == "string"){return true} else {return false}}
function toArray(ele){
	if(ele == ""){return null}
	if(isString(ele)){return @Explode(ele, ",")}
	return ele	
}
function sortList(listValue){if (listValue==null){return ""} else if (listValue.constructor == Array){return listValue.sort()} else {return listValue}}

function IsNullorBlank(strValue){	
	strValue = @Trim(strValue);	
	if(typeof strValue == "java.util.Vector"){if(strValue.join('') == ''){return true}}
	if (strValue == 0){return true}
	if (strValue == null){return true}
	if (strValue == "" & isString(strValue)){return true}
	if(strValue.length == 0){return true}
	return false
}

function IsFieldNull(docX, strField){
	var docX:NotesXspDocument = docX;
	if(docX.hasItem(strField) == false){return true}
	return docX.getItemValue(strField).isEmpty();
}

function IsDateTimeString(strValue, strFormat){
	
	var sdf:java.text.SimpleDateFormat = new java.text.SimpleDateFormat(strFormat); 
	sdf.setLenient(false);	
	
	try {
		var date:java.util.Date = sdf.parse(strValue);	
	} catch (e) {		
		return false
	}	
	return true	
}

function ChangePassword_Submit(){
	
	//Get NAB Document for this user
	var docNAB:NotesDocument = GetDB_NAB().getView('($Users)').getDocumentByKey(General_GetUser_DocKey(), true);
	if(docNAB == null){
		Global_Notify("ERROR: Could not find User Server Document"); 
		return
	}
		
	var doc:NotesDocument = GetDB_Database('USER').getDocumentByUNID(General_GetUser_UNID());
	if(doc == null){
		Global_Notify("ERROR: Could not find User Document"); 
		return
	}
	
	var strPwd_Curr = getComponent('PWD_Curr').getValue();
	var strPwd_New = getComponent('PWD_New').getValue();
	var strPwd_Confirm = getComponent('PWD_Confirm').getValue();
	
	// Validate Fields
	if(IsNullorBlank(strPwd_Curr) | IsNullorBlank(strPwd_New) | IsNullorBlank(strPwd_Confirm)){
		Global_Notify("Error: Please complete all the fields");
		return
	} 
	
	if(strPwd_New != strPwd_Confirm){Global_Notify("Error: Confirm Password does not match with New Password"); return}
	if(strPwd_New == strPwd_Curr){Global_Notify("Error: New Password can not be same as Current Password"); return}
	if(sessionAsSigner.verifyPassword(strPwd_Curr, docNAB.getItemValueString('HTTPPassword')) == false){
		Global_Notify("Error: Your current password is not correct"); return
	}
	
	//All Good Update the Document
	docNAB.replaceItemValue("HTTPPassword", strPwd_New);
	docNAB.replaceItemValue("CustomPassword", strPwd_New);	
	docNAB.computeWithForm(true, false);
	docNAB.save(true, false);
	docNAB.recycle();
		
	doc.replaceItemValue("USER_Password", strPwd_New);
	doc.save(true, false);
	doc.recycle();
	viewScope.put("CP_Success", "YES");	
}

function Calendar_Interpreter_List(){
	
	if(param.Lang){return @DbLookup(GetDB_FilePath('USER'), "USER_byLanguage", param.Lang, 2, '[FAILSILENT]')}
	
	var vRetList = [];
	if(IsUser_Manager() | IsUser_Coordinator() | IsUser_DBAdmin()){
		var vRetList = @DbColumn(GetDB_FilePath('USER'), "USER_byLanguage", 2);		
	} else {
		var vLList = ssGetVariable('ssUser_LanguageList');		
		for(var i=0;i<vLList.length; i++){
			var vList = @DbLookup(GetDB_FilePath('USER'), "USER_byLanguage", vLList[i], 2);
			if(isString(vList)){vRetList.push(vList)} else {for(var j=0;j<vList.length;j++){vRetList.push(vList[j])}}	
		}
	}		
	return @Unique(vRetList.sort());
}

function Calendar_Interpreter_List_DefaultValue(){
	if(param.idk){
		return param.idk			
	} else if(compositeData.defaultValue){
		return compositeData.defaultValue
	} else {
		return null
	}	
}

function User_MyBooking_DefaultScreen_Update(){
	var docUSER:NotesDocument = GetDB_Database('USER').getDocumentByUNID(General_GetUser_UNID());
	if(docUSER != null){
		docUSER.replaceItemValue('User_MyBooking_DefaultScreen', User_MyBooking_DefaultScreen());
		docUSER.save(true, false);		
	}
}

function User_MyBooking_Heading(){
	var strView = User_MyBooking_DefaultScreen();
	if(strView == 'Calendar'){return 'Calendar'}
	if(strView == 'CurrentBookings'){return 'Current Bookings'}
	if(strView == 'AllBookings'){return 'All Bookings'}
}

function User_MyBooking_DefaultScreen(bSavedValue){	
	if(sessionScope.containsKey('User_MBDS') == false | bSavedValue){
		var strView = "CurrentBookings"
		var docUSER:NotesDocument = GetDB_Database('USER').getDocumentByUNID(General_GetUser_UNID());
		if(docUSER != null){
			if(docUSER.hasItem('User_MyBooking_DefaultScreen')){
				strView = docUSER.getItemValueString('User_MyBooking_DefaultScreen');
				if(bSavedValue){return strView} //this will return stored value
			}
		}
		if(bSavedValue){return ''}
		sessionScope.put('User_MBDS', strView);
	}
	return sessionScope.User_MBDS
}

function InterpreterList_ByMultipleLanguage(vLanguage){
	
	var strFormula = 'Form = "USER" & DEACTIVE != "YES" & (@Contains(USER_Role_Primary;"Interpreter") | @Contains(USER_Role_Secondary;"Interpreter"))';
	//& @Contains(USER_Languages; "Cantonese") & @Contains(USER_Languages; "Vietnamese")
	strFormula += ' & @Contains(USER_Languages; "' + vLanguage.join('") & @Contains(USER_Languages; "') + '")';
	//print(strFormula);	
	var db:NotesDatabase = GetDB_Database('USER');
	var dc:NotesDocumentCollection = db.search(strFormula, null, 0);
	if (dc.getCount() == 0){return null}
	var doc:NotesDocument = dc.getFirstDocument();
	var vReturn = [];
	while(doc != null){		
		vReturn.push(doc.getItemValueString('USER_FullName') + '|' + doc.getItemValueString('DocKey'))		
		doc = dc.getNextDocument(doc);
	}
	return vReturn.sort();
}

function User_AllBooking_Interpreter_List(bCount){
	
	var vLList = ssGetVariable('ssUser_LanguageList');
	var vRetList = [];
	for(var i=0;i<vLList.length; i++){
		var vList = @DbLookup(GetDB_FilePath('USER'), "USER_byLanguage", vLList[i], 2);
		if(isString(vList)){
			if(vList.indexOf(General_GetUser_DocKey())<0){vRetList.push(vList)}
		} else {
			for(var j=0;j<vList.length;j++){
				if(vList[j].indexOf(General_GetUser_DocKey())<0){vRetList.push(vList[j])}	
			}
		}	
	}	
	if(bCount){return vRetList.length} else {return @Unique(vRetList.sort())}
}

function USER_Leaves(bUpdate){
	
	//Sample - [[201712040000, 201712050000], [201712250700, 201712261800]]
	
	var strKey = "hmUserLeaves";	
	
	if(bUpdate | applicationScope.containsKey(strKey) == false){
		var nvec:NotesViewEntryCollection = GetDB_Database('USER').getView('USER_LEAV_AppScope').getAllEntries();
		var vHM:java.util.HashMap = new java.util.HashMap(); 
		if(nvec.getCount() == 0){
		 	vHM.put('NULL','NULL');
		} else {			
			var nve:NotesViewEntry = nvec.getFirstEntry();
			var strDocKey; var vLeaves;
			var intNow = parseFloat(I18n.toString(@Now(), "yyyyMMddHHmm"));
					
			while(nve != null){
			
				strDocKey = nve.getColumnValues().elementAt(0);
				vLeaves = @TextToNumber(nve.getColumnValues().elementAt(2).split('~'));
																
				if(vLeaves[1] >= intNow){					
					
					var vExistValues:java.util.Vector = new java.util.Vector;
					if(vHM.containsKey(strDocKey)){vExistValues = vHM.get(strDocKey)}
					vExistValues.add(vLeaves);				
					vHM.put(strDocKey, vExistValues);
				}				
				nve = nvec.getNextEntry(nve);
			}			
		}		
		applicationScope.put(strKey, vHM);
		print('~~~~~~~~~~ INFO: Application Scope - ' + strKey + ' is updated...')
	}
	
	return applicationScope.get(strKey);	
}

function USER_Hours_Standard(){
	
	var strKey = "Global_StandardHours";
	if(applicationScope.containsKey(strKey) == false){
		var vList = GetKeyword(strKey);
		var vHM:java.util.HashMap = new java.util.HashMap();
		for(var i=0; i<(vList.length-1); i++){
			vHM.put(@TextToNumber(@Left(vList[i], "~")), @TextToNumber(@Right(vList[i], "~").split("-")))
		}		
		applicationScope.put(strKey, vHM);
	}
	return applicationScope.get(strKey);
}

function USER_Hours_NonStandard(bUpdate){
	
	//Sample - [[900.0, 1600.0], [1300.0, 1800.0], 0.0, 0.0, [1300.0, 1700.0], 0.0, 0.0]
	var strKey = "hmUserHoursNS";	
	
	if(bUpdate | applicationScope.containsKey(strKey) == false){
		var nvec:NotesViewEntryCollection = GetDB_Database('USER').getView('USER_Hours_NS').getAllEntries();
		var vHM:java.util.HashMap = new java.util.HashMap(); 
		if(nvec.getCount() == 0){
		 	vHM.put('NULL','NULL');
		} else {			
			var nve:NotesViewEntry = nvec.getFirstEntry();
			var strDocKey;
			while(nve != null){
				var vValues:java.util.Vector = nve.getColumnValues();
				strDocKey = vValues[0];
				vValues.removeElementAt(0);
				for(var i=0; i<vValues.size(); i++){
					vValues.setElementAt(@TextToNumber(@ReplaceSubstring(vValues[i], ":", "").split('~')), i) 
				}			
				vHM.put(strDocKey, vValues);				
				nve = nvec.getNextEntry(nve);
			}			
		}		
		applicationScope.put(strKey, vHM);
		//print('~~~~~~~~~ INFO: Application Scope - ' + strKey + ' is updated...')
	}	
	return applicationScope.get(strKey);	
}

function USER_Hours_Extra(bUpdate){
	
	//Date, DateTimeStart_1, DateTimeEnd_1, DateTimeStart_2, DateTimeEnd_2
	//Sample - [[20171204, 201712040000, 201712040000, 0, 0], [20171225, 201712250700, 201712251200, 201712251500, 201712251800]]
	
	var strKey = "hmUserHours_DT";	
	
	if(bUpdate | applicationScope.containsKey(strKey) == false){
		var nvec:NotesViewEntryCollection = GetDB_Database('USER').getView('USER_HRS_DT_AppScope').getAllEntries();
		//print(nvec.getCount() + '~~~~~~~~~~~~~~~~~~~')
		var vHM:java.util.HashMap = new java.util.HashMap(); 
		if(nvec.getCount() == 0){
		 	vHM.put('NULL','NULL');
		} else {			
			var nve:NotesViewEntry = nvec.getFirstEntry();
			var strDocKey; var strDt;
			var intToday = parseFloat(I18n.toString(@Now(), "yyyyMMdd"));
					
			while(nve != null){
			
				strDocKey = nve.getColumnValues().elementAt(0);
				strDt = @TextToNumber(nve.getColumnValues().elementAt(1));
																
				if(strDt >= intToday){
					var vExistValues:java.util.Vector = new java.util.Vector;
					if(vHM.containsKey(strDocKey)){vExistValues = vHM.get(strDocKey)}
					var vNewValues:java.util.Vector = nve.getColumnValues();
					vNewValues.removeElementAt(0);
					vExistValues.add(@TextToNumber(vNewValues));				
					vHM.put(strDocKey, vExistValues);
				}				
				nve = nvec.getNextEntry(nve);
			}			
		}		
		applicationScope.put(strKey, vHM);
		print('~~~~~~~~~~ INFO: Application Scope - ' + strKey + ' is updated...');
	}
	
	return applicationScope.get(strKey);	
}

function ConvDt(strDate){
	var dt:Date = new Date();
	dt.setFullYear(@Left(strDate,4), (@Middle(strDate,4,2)-1), @Middle(strDate,6,2));
	if(@Middle(strDate,8,2) == "00" & @Right(strDate,2) == "00"){
		return I18n.toString(dt, "MMM dd, yyyy")	
	}
	dt.setHours(@Middle(strDate,8,2), @Right(strDate,2))
	return I18n.toString(dt, "MMM dd, yyyy HH:mm")	
}

function KWD_Field_IsEditable(strFieldName){
	if(document1.isEditable() == false){return false}
	if(IsUser_DBAdmin()){return true}	
	if(strFieldName == 'KWText' | strFieldName == 'KWMultiText'){return true}
	return false
}

function REQU_DefaultScreen_Update(){
	var docREQU:NotesDocument = GetDB_Database('REQU').getDocumentByUNID(General_GetUser_UNID());
	if(docREQU != null){
		docREQU.replaceItemValue('REQU_DefaultScreen', REQU_DefaultScreen());
		docREQU.save(true, false);		
	}
}

function REQU_DefaultScreen(bSavedValue){	
	if(sessionScope.containsKey('REQU_MBDS') == false | bSavedValue){
		var strView = "GRID_REQU_ByDocKey"
		var docREQU:NotesDocument = GetDB_Database('REQU').getDocumentByUNID(General_GetUser_UNID());
		if(docREQU != null){
			if(docREQU.hasItem('REQU_DefaultScreen')){
				strView = docREQU.getItemValueString('REQU_DefaultScreen');
				if(bSavedValue){return strView} //this will return stored value
			}
		}
		if(bSavedValue){return ''}
		sessionScope.put('REQU_MBDS', strView);
	}
	return sessionScope.REQU_MBDS
}

function BOOK_JSON_TimeDifference(){
	
	//Assumption Booking Date cannot be on Weekend and Holidays
	var externalContext = facesContext.getExternalContext();
	var writer = facesContext.getResponseWriter();
	var response = externalContext.getResponse();
 
	// Set content type
	response.setContentType("application/json");
	response.setHeader("Cache-Control", "no-cache");
	
	var strFld = param.Fld;
	var vDate = param.Dt.split['~'];	//Booking Date
	
	var keyHours = "24";
	if(strFld == "BOOK_Flexible"){keyHours = GetKeyword('BOOK_Flexible_NoticeHours')}
	keyHours = @TextToNumber(keyHours);
	
	var dtNow = new Date();
	var dtBook:Date = @Date(vDate[0], vDate[1], vDate[2], vDate[3], vDate[4], 0);
	
	var strReturn = 0;
	if(dtBook > dtNow){
		
	}
		
	writer.write("{\"hours\":" + strReturn + "}");
	writer.endDocument();	
}



function USER_HRS_DT_AddEdit(strDate, strDocKey, strLunch){
	
	var strTimeStart_1 = getComponent('fld_NewTime_Start_1').getValue();
	var strTimeEnd_1 = getComponent('fld_NewTime_End_1').getValue();
	var strTimeStart_2 = getComponent('fld_NewTime_Start_2').getValue();
	var strTimeEnd_2 = getComponent('fld_NewTime_End_2').getValue();
	var splitHours = getComponent('fld_SplitHours').getValue();
		
	if(IsNullorBlank(strTimeStart_1) || IsNullorBlank(strTimeEnd_1)){print('ERROR: values are blank'); return 'ERROR'}
	if(splitHours == "Y"){
		if(IsNullorBlank(strTimeStart_2) || IsNullorBlank(strTimeEnd_2)){print('ERROR: values are blank'); return 'ERROR'}
	}	
	if(IsNullorBlank(strDate)){print("ERROR: Booking Date is blank"); return 'ERROR'}
	
	var ndtS:NotesDateTime = session.createDateTime(strDate + " " + strTimeStart_1);
	var ndtE:NotesDateTime = session.createDateTime(strDate + " " + strTimeEnd_1);
	
	var strFormula = 'Form = "USER_HRS_DT" & DocKey_Parent = "' + strDocKey + '" & HRS_DT = "' + I18n.toString(ndtS.toJavaDate(), 'yyyyMMdd') + '"';
	var dc:NotesDocumentCollection = GetDB_Database('USER').search(strFormula, null, 0);
	
	if(dc.getCount() == 0){
		
		var docHR:NotesDocument = GetDB_Database('USER').createDocument();
		docHR.replaceItemValue('Form', 'USER_HRS_DT');
		docHR.replaceItemValue('DocKey', AppForm_DocKey('HRS_DT'));
		docHR.replaceItemValue('DocKey_Parent', strDocKey);
		docHR.replaceItemValue('DocKey_CreatedBy', General_GetUser_DocKey());
		docHR.replaceItemValue('CreatedDate', session.createDateTime(@Today()).getDateOnly());
		docHR.replaceItemValue('CreatedBy', General_GetUser_FullName());
		
	} else {
		
		var docHR:NotesDocument = dc.getFirstDocument();
		docHR.replaceItemValue('UpdatedBy', General_GetUser_FullName());		
	}
	
	docHR.replaceItemValue('HRS_DT', I18n.toString(ndtS.toJavaDate(), 'yyyyMMdd'));
	docHR.replaceItemValue('HRS_DT_Start_1', I18n.toString(ndtS.toJavaDate(), 'HHmm'));
	docHR.replaceItemValue('HRS_DT_End_1', I18n.toString(ndtE.toJavaDate(), 'HHmm'));
	docHR.replaceItemValue('HRS_DT_Hours_1', ndtE.timeDifference(ndtS)/3600);
	docHR.replaceItemValue('HRS_DT_Split', splitHours);
	var hoursDiff = (ndtE.timeDifference(ndtS)/3600);
	
	if(splitHours == "Y"){
		
		ndtS = session.createDateTime(strDate + " " + strTimeStart_2);
		ndtE = session.createDateTime(strDate + " " + strTimeEnd_2);
		hoursDiff += (ndtE.timeDifference(ndtS)/3600);
		
		docHR.replaceItemValue('HRS_DT_Start_2', I18n.toString(ndtS.toJavaDate(), 'HHmm'));	
		docHR.replaceItemValue('HRS_DT_End_2', I18n.toString(ndtE.toJavaDate(), 'HHmm'));
		docHR.replaceItemValue('HRS_DT_Hours_2', ndtE.timeDifference(ndtS)/3600);
	}
		
	docHR.replaceItemValue('HRS_DT_Lunch', strLunch);
	//No. of Hours
	docHR.replaceItemValue('HRS_DT_Hours_Total', hoursDiff);
	print("save hours")
	docHR.save(true, false);
	
	//Update Application Scope
	USER_Hours_Extra(true);
	return "OK"
}

function Date_ChangeFormat(dtString, strInFormat, strOutFormat){
	dtString = @Text(dtString);
	var dtFrmt_In:java.text.SimpleDateFormat =  new java.text.SimpleDateFormat( strInFormat );
	var dtObject = dtFrmt_In.parse(dtString);
	//Remove Hours & Minutes if zeros
	if(strInFormat == "yyyyMMddhhmm" & @Right(dtString, 4) == "0000"){strOutFormat = @ReplaceSubstring(strOutFormat, " hh:mm", "")}
	
	var dtFrmt_Out:java.text.SimpleDateFormat =  new java.text.SimpleDateFormat( strOutFormat );
	
	/*if(@Right(dtString, 4) == "0000"){
		dtS = @Left(dtS, 11);
		print(dtS)
	}*/
	
	return dtFrmt_Out.format(dtObject);	
}
function BOOK_NewBookingURN_IMS(){	
	var vVal = @Trim(getComponent('fldPatientURN_IMS').getValue()).split(" : ");
	/**
	 * Condition added by Baljit on 20 May 2019
	 * Based on KW to fetch patient data from patient database
	 */
	var searchPatientDB = (GetKeyword("SearchPatientFromPatientDB").equalsIgnoreCase("Yes"))? true:false;	
	if (searchPatientDB) {
		var strFormula = 'Form = "Patient" & !@IsAvailable($Conflict) & UrNumber = "' + vVal[0] + '"';
		strFormula += ' & FirstName = "' + vVal[1] + '" & LastName = "' + vVal[2] + '"';
		if(vVal.length == 4){strFormula += ' & Gender = "' + vVal[3] + '"';}	
		var dc:NotesDocumentCollection = GetDB_Database('Patient').search(strFormula, null, 0);
		if(dc.getCount() == 0){print('ERROR: Cannot find any document/s with formula - ' + strFormula);return}
		sessionScope.put('IMS_PatientDataFromPDB', dc.getLastDocument().getUniversalID());
	} else {	
		var strFormula = 'Form = "BOOK" & !@IsAvailable($Conflict) & BOOK_PatientUR = "' + vVal[0] + '"';
		strFormula += ' & BOOK_PatientFirstName = "' + vVal[1] + '" & BOOK_PatientLastName = "' + vVal[2] + '"';
		if(vVal.length == 4){strFormula += ' & BOOK_PatientGender = "' + vVal[3] + '"';}
		var dc:NotesDocumentCollection = GetDB_Database('BOOK').search(strFormula, null, 0);
		if(dc.getCount() == 0){print('ERROR: Cannot find any document/s with formula - ' + strFormula);return}
		sessionScope.put('IMS_PatientData', dc.getLastDocument().getUniversalID());
	}
}

function CBN_Enabled(){return GetKeyword('CBN_Enable') == 'YES'}
/**
 * To display a custom message for some occasions below header
 */
function isMessageBoxEnabled() {
	return GetKeyword('Alert_EnableMessageBox').toUpperCase() == 'YES';
}
/**
 * Get message title and Message for broadcasting a message to IMS Users
 */
function getMessageBoxHTML() {
	var strMessageTitle = GetKeyword('Alert_MessageTitle');
	var strMessage = GetKeyword('Alert_Message');
	var strHTML_Return = '';
	if (strMessage != null && !strMessage.equalsIgnoreCase('')) {
		strHTML_Return = '<div class="container container-fluid"><div class="panel panel-info message-panel">';
		if (strMessageTitle != null && !strMessageTitle.equalsIgnoreCase('blank')) {
			strHTML_Return += '<div class="panel-heading" role="button" data-toggle="collapse" href="#messageBox" aria-expanded="true" aria-controls="messageBox"><h4 class="panel-title">' + strMessageTitle + '</h4></div>';
		}
		strHTML_Return += '<div class="panel-body collapse in" id="messageBox">' + strMessage + '</div></div>';
		strHTML_Return += '</div></div>';
	}
	return strHTML_Return;
}

/**
 * Check if oncall amend api is enabled, add to view to use it at various pages
 */
function isEnabledOnCallAmendAPI() {
	return (GetKeyword('OnCall_EnableAmendAPI') != null && GetKeyword('OnCall_EnableAmendAPI').equalsIgnoreCase("Yes")) ? true: false;
}





