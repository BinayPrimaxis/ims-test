var mtChart = null; var dyChart = null; var rcnData = null; var rcnProgress = null; var smap=null;
var chartIndex = {};
function Chart_Month(vRange){
	
	if(!vRange){
		var dtLast = new Date();
		var dtFirst = new Date();
		dtFirst.setMonth(dtLast.getMonth()-11);
	} else {
		var dtFirst = new Date('01 ' + vRange[0]);
		var dtLast = new Date('01 ' + vRange[1]);
	}
	
	var strKey = 'Month_' + GetYearMonth(dtFirst) + "-" + GetYearMonth(dtLast);
	cLog("Chart_Month ... Key - " + strKey)
	$('.chartMonth_Heading').html('Bookings - ' + GetHeadingMY(dtFirst) + ' till ' + GetHeadingMY(dtLast));
	
	$.ajax({		
		url: "xJson_Data.xsp?ChartKey=" + strKey,		
		success: function(result){
			var chartOptions = {
				legendCallback: function(chart) {return Chart_Legend(mtChart, "M")},                                                                                     
				legend: {display: false},
				scales : {xAxes:[{gridLines:{display:false}}], yAxes: [{ticks: {beginAtZero:true}}]},
				onClick: function(evt){
		        	try{
		        		var linePoints = mtChart.getElementsAtEvent(evt);
				       	var xindex = linePoints[0]._index
				       	var strLabel = linePoints[0]._xScale.ticks[xindex];				     
				       	Chart_Day(strLabel);
		        	} catch(e){
		        		cLog(e)
		        	}
		        }
			};                                                                                         
			// Show/hide chart by click legend
			updateDataset = function(e, datasetIndex, strType) {Chart_Legend_OnClick(e, datasetIndex, strType)};
			//Generate Chart
			var mtx = document.getElementById("homeChart_Month").getContext("2d");
			if(mtChart != null){mtChart.destroy()}
			mtChart = new Chart(mtx, {type: 'line', data: result, options: chartOptions});
			$('.homeChart_Month_Legend').html(mtChart.generateLegend());
		},		
		error: function(xhr,status,error){alert("ERROR= " + error)}
	});
}

function Chart_Day(strRange){
	
	if(!strRange){
		var dt = new Date();
	} else {
		//cLog(strRange + '~~~~~~~~~~~~~~~~~~~~~~~')
		//Sample Oct 16 (IE has issues with month name
		strRange = strRange.split(' ');
		if(strRange[1].length == 2){
			var dt = new Date('01 ' + strRange[0] + ' 20' + strRange[1])
		} else {
			var dt = new Date('01 ' + strRange[0] + ' ' + strRange[1])
		}		
	}
	
	var strKey = 'Day_' + GetYearMonth(dt);
	//cLog("Chart_Day ... Key - " + strKey)
	$('.chartDay_Heading').html('Bookings - ' + GetHeadingMY(dt));
	
	$.ajax({		
		url: "xJson_Data.xsp?ChartKey=" + strKey,		
		success: function(result){
			var chartOptions = {
				legendCallback: function(chart) {return Chart_Legend(dyChart, "D")},
				scales : {xAxes:[{gridLines:{display:false}}], yAxes: [{ticks: {beginAtZero:true}}]},
				legend: {display: false}								
			};                                                                                         
			// Show/hide chart by click legend
			updateDataset = function(e, datasetIndex, strType) {Chart_Legend_OnClick(e, datasetIndex, strType)};
			//Generate Chart
			var mtx = document.getElementById("homeChart_Day").getContext("2d");
			if(dyChart != null){dyChart.destroy()}
			dyChart = new Chart(mtx, {type: 'line', data: result, options: chartOptions});
			$('.homeChart_Day_Legend').html(dyChart.generateLegend());
		},		
		error: function(xhr,status,error){alert("ERROR= " + error)}
	});
}

function Chart_Legend(chart, strType){	
	
	var legendHtml = [];
	
	for (var i=0; i<chart.data.datasets.length; i++) {
		
		var bgColor = chart.data.datasets[i].backgroundColor;
		var strStyle = 'background-color:' + bgColor + '; border-color:inherit;'
		var strOnClick = 'updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\',\'' + strType + '\')'
		var vTemp = bgColor.split(","); 
		vTemp[vTemp.length-1] = "6)";
		var strStyleDiv = 'margin-bottom: 3px; border-color:' + vTemp.join(",")
		
    	var strTemp = '<div class="input-group input-group-sm" data-toggle="buttons" style="' + strStyleDiv + '">';
		strTemp += '<label class="btn active input-group-addon cbChart" style="' + strStyle + '" onclick="'+ strOnClick +'">';
		strTemp += '<input type="checkbox" checked>'
		strTemp += '<span class="glyphicon glyphicon-ok" /></label>';
		//console.log(chart.data.datasets[i].label + " - " + chart.data.datasets[i].labelTotal)
		strTemp += '<span style="border-color:inherit" class="form-control">' + chart.data.datasets[i].label + '&nbsp;&nbsp;&nbsp;';
		strTemp += '(' + chart.data.datasets[i].labelTotal + ')';
		strTemp += '</span></div>';
		
		legendHtml.push(strTemp);		
    }
	
    return legendHtml.join("");
}

function Chart_Legend_OnClick(e, datasetIndex, strType){	
	//cLog(strType)	
	var ci = (strType == "M") ? e.view.mtChart : e.view.dyChart;	
	var meta = ci.getDatasetMeta(datasetIndex);
	// See controller.isDatasetVisible comment
	meta.hidden = meta.hidden === null? !ci.data.datasets[datasetIndex].hidden : null;
	//We hid a dataset ... rerender the chart
	ci.update();	
}

function GetYearMonth(dt){return moment(dt).format('YYYYMM')}
function GetHeadingMY(dt){return moment(dt).format('MMMM YYYY')}

function GetMonthRange(strStart, fieldClass, fieldClass_Day){	
	
	//This means From Value is blank now.. so set Till Value to remove all options
	if(strStart == ''){
		$('.fld_ChartMonth_Till').children('option:not(:first)').remove();
		return
	}	
	var dtStart = new Date("01 " + strStart);	
	var dtEnd = new Date();
	//From Range will be one month less than current month
	if(fieldClass == "ChartMonth_From"){dtEnd.setMonth(dtEnd.getMonth()-1)}
	if(fieldClass == "ChartMonth_Till"){dtStart.setMonth(dtStart.getMonth()+1)}
	
	var intEnd = parseInt(GetYearMonth(dtEnd)); var intStart = parseInt(GetYearMonth(dtStart));
	
	var vList = [];
	while (intStart <= intEnd){
		vList.push(GetHeadingMY(dtStart));
		dtStart.setMonth(dtStart.getMonth() + 1);
		intStart = parseInt(GetYearMonth(dtStart));
	}
	
	$.each(vList, function(key, value) {
	     $('.fld_' + fieldClass)
	         .append($("<option></option>")
	         .attr("value", value)
	         .text(value));
	});
	
	//This is one time on load for the Day Chart to populate the list of month/year
	if(fieldClass_Day){
		$.each(vList, function(key, value) {
		     $('.fld_' + fieldClass_Day)
		         .append($("<option></option>")
		         .attr("value", value)
		         .text(value));
		});
	}
}

function Chart_Month_EditRange_Submit(){
	
	var strFrom = $('.fld_ChartMonth_From').val(); var strTill = $('.fld_ChartMonth_Till').val();	
	if(strFrom == "" || strTill == ""){
		Notify_Warning("Please select Range in both 'Month From' & 'Month Till'"); return
	}
	
	var dtFrom = new Date("01 " + strFrom); var dtTill = new Date("01 " + strTill);
	if(dtFrom.getTime() >= dtTill.getTime()){
		Notify_Warning("Month from should be less than Month till. Please choose again."); return
	}
	
	Chart_Month([strFrom, strTill]);
	$('#dlg_ChartMonthEdit').modal('hide');
	$('.fld_ChartMonth_From').val('');
	$('.fld_ChartMonth_Till').children('option:not(:first)').remove();
}

function Chart_Day_Edit_Submit(){	
	var strMonth = $('.fld_ChartDay').val();	
	if(strMonth == ""){Notify_Warning("Please select Month to change Chart"); return}	
	Chart_Day(strMonth);
	$('#dlg_ChartDayEdit').modal('hide');
	$('.fld_ChartDay').val('');
}

var reportCharts = [];

function Report_ChartGenerate(bLoad){
	
	if(reportCharts.length > 0){
		for(var i=0; i<reportCharts.length; i++){reportCharts[i].destroy()}
	}	
	if(!bLoad){return}
	
	var vData = $('.chartData').html();
	if(vData.length == 0){return}
	vData = eval(vData);
	
	Report_GetChart(vData[0], 'reportChart_Summary');
	Report_GetChart(vData[1], 'reportChart_PatientType');
	Report_GetChart(vData[2], 'reportChart_Covered');
	Report_GetChart(vData[3], 'reportChart_Uncovered');	
	Report_GetChartBar(vData[4], 'reportChart_Site');	
	smap=XSP.fromJson($(".smap").val());
	//reportCharts[4].options.maintainAspectRatio=false
	$("#reportChart_Site").click( 
            function(evt){
            	var activePoints = reportCharts[4].getElementsAtEvent(evt);
            	console.log(evt);
            	console.log(reportCharts)
            	console.log(activePoints);
               $(".sreport>.active").removeClass("active");
            	$("#"+smap[activePoints[0]._model.label]).addClass("active")
            })
}

function Report_GetChart(vDataItem, strID,colourJson){
	console.log(vDataItem)
	console.log(vDataItem.data)
	
	var bgColours = ['rgba(255,98,98, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)','rgba(243, 121, 0, 1)', 'rgba(153, 102, 155, 1)'];
	if (colourJson)	
		{
		bgColours = []
		for (var i=0;i<vDataItem.label.length;i++){
			bgColours.push(colourJson[vDataItem.label[i]])
		}
		}
	console.log(bgColours)
	var configChart = {
		type: 'doughnut',		
		data: {
			datasets: [{
				data : vDataItem.data,
				backgroundColor: bgColours					
			}],
			labels: vDataItem.label
		},
		options: {
			responsive: true
			,title: {display: false, text: vDataItem.title}
			,animation: {animateScale: true, animateRotate: true}
			,legend: {display:true, position: 'right'}
			//,legendCallback: function(chart) {return Report_Chart_Legend(chart);}			
		}
	};
	var chartObj = new Chart(document.getElementById(strID).getContext("2d"), configChart);
	
	reportCharts.push(chartObj);
	chartIndex[strID]=reportCharts.length-1
	//$('#' + strID + '_Legend').html(chartObj.generateLegend());
	//cLog(chartObj.generateLegend());
}

function Report_GetChartBar(vDataItem, strID){
	console.log(vDataItem)
	console.log(vDataItem.data)
	var bgColours = ['rgba(255,98,98, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 206, 86, 1)', 'rgba(75, 192, 192, 1)', 'rgba(153, 102, 255, 1)','rgba(243, 121, 0, 1)', 'rgba(153, 102, 155, 1)'];
		
	var configChart = {
		type: 'bar',		
		data: {
			datasets: [{
				data : vDataItem.data				
			}],
			labels: vDataItem.label
		},
		options: {
			responsive: true
			,scales: {
	            yAxes: [{
	                ticks: {
	                    beginAtZero:true
	                }
	            }],
	            xAxes: [{
	                ticks: {
                    beginAtZero:true,
                    autoSkip:false
                }
            }]
	        }
			,title: {display: false, text: vDataItem.title}
			,animation: {animateScale: true, animateRotate: true}
			,legend: {display:false, position: 'right'}
			//,legendCallback: function(chart) {return Report_Chart_Legend(chart);}			
		}
	};
	var chartObj = new Chart(document.getElementById(strID).getContext("2d"), configChart);
	
	reportCharts.push(chartObj);
	
	//$('#' + strID + '_Legend').html(chartObj.generateLegend());
	//cLog(chartObj.generateLegend());
}
function Report_Chart_Legend(chart){	
	
	var legendHtml = [];
	var data = chart.data.datasets[0].data;
	var bgColor = chart.data.datasets[0].backgroundColor;
	var labels = chart.data.labels;
	//cLog(data);
	var totCount = 0
	for (var i=0; i<data.length; i++) {totCount += data[i]}
	
	var strHTML;
	for (var i=0; i<data.length; i++) {
		strHTML = '<div style="background-color:' + bgColor[i] + '; border-color:inherit; width:20px">';
		strHTML += ((data[i]/totCount)*100).toFixed(1) + '%</div>';
		legendHtml.push(strHTML);
	}
	//cLog(legendHtml)
	return legendHtml.join("");
		
}

function RCN_ProgressUpdate(iCount)
{
	$(".refreshProgress").click()
	}

function reportUpdate(newData, strID)
{
	console.log(newData)
	var c=reportCharts[chartIndex[strID]];
	console.log(c);

	c.data.datasets[0].data = newData.data
	c.data.labels = newData.label
	c.update();		
}


