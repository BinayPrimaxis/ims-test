var gbl_Planner_SCKey = 'scPlannerData';
var gbl_Planner_SCKey_Date = 'scPlannerDates';

function Planner_EditAllowed(){	
	if(IsUser_DBAdmin()){return true}
	if(IsUser_Manager()){return true}	
	var strCoordinatorAccess = GetKeyword('Planner_Coordinator_Access');
	if(strCoordinatorAccess == 'W' | strCoordinatorAccess == 'D'){return true}	
	return false	
}

function Interpreter_Filter_Add(){
	
	var docX:NotesXspDocument = document1;
	var fldName = 'Planner_Interpreter_Name'; var fldValue = GetValue(fldName);				
	if(IsNullorBlank(fldValue)){view.postScript("Notify_Warning('Enter Interpreter Name')"); return}
	
	var vFList:Array = []; var strFld_Selected = 'Planner_Interpreter_Selected';
	if(docX.hasItem(strFld_Selected)){vFList = docX.getItemValue(strFld_Selected)}
	
	if(vFList.join('').indexOf(fldValue)>=0){
		view.postScript("Notify_Warning('Interpreter has been already added')");
	} else {
		vFList.push(fldValue);
		vFList.sort();
		docX.replaceItemValue(strFld_Selected, vFList);
	}
	docX.removeItem(fldName);
}

function Interpreter_Filter_Remove(intPos){	
	var docX:NotesXspDocument = document1;
	var vFList:java.util.Vector = docX.getItemValue('Planner_Interpreter_Selected');
	vFList.remove(intPos)
	if(vFList.join('').length == 0){vFList = null}
	docX.replaceItemValue('Planner_Interpreter_Selected', vFList);	
	view.postScript("Notify_Info('Interpreter Removed')")	
}

function Planner_Generate(){
	
	var docX:NotesXspDocument = document1;
	if(sessionScope.containsKey(gbl_Planner_SCKey)){sessionScope.remove(gbl_Planner_SCKey)}	
	if(sessionScope.containsKey(gbl_Planner_SCKey_Date)){sessionScope.remove(gbl_Planner_SCKey_Date)}
	
	var dtRange = GetValue('Planner_DateRange');
	if(IsNullorBlank(dtRange)){view.postScript("Notify_Warning('Select Dates')"); return}
	var dtTemp = @Trim(dtRange.split(" - "));
	
	var dtStart:Date = I18n.parseDate(dtTemp[0], "dd MMM yyyy"); var dtEnd:Date = I18n.parseDate(dtTemp[1], "dd MMM yyyy");
	var intStart = parseFloat(I18n.toString(dtStart, "yyyyMMdd")); var intEnd = parseFloat(I18n.toString(dtEnd, "yyyyMMdd"));
	if (intEnd < intStart){
		view.postScript("Notify_Warning('End Date is less than Start Date, please rectify')"); return
	}
	//Make the Date Array
	var vDates:java.util.Vector = new java.util.Vector();
	var intWDay;
	while (intEnd>=intStart){
		intWDay = @Weekday(dtStart);
		if(intWDay != 1 & intWDay != 7){
			vDates.add(dtStart);
		}	
		dtStart = @Adjust(dtStart, 0, 0, 1, 0, 0, 0);
		intStart = parseFloat(I18n.toString(dtStart, "yyyyMMdd"));
	}
	
	sessionScope.put(gbl_Planner_SCKey_Date, vDates);
	//print(vDates);
	
	//Edit Control
	var bEditAllowed = Planner_EditAllowed();
	//if True, then check for date control
	if(bEditAllowed){
		var intYM_Allow = parseFloat(I18n.toString(@Adjust(@Today(), 0, -1, 0, 0, 0, 0), "yyyyMM"));
		if(parseFloat(I18n.toString(dtStart, "yyyyMM")) < intYM_Allow){
			bEditAllowed = false;
		}
	}
	viewScope.put("Planner_Edit", bEditAllowed);
	getComponent('scPlanner_EditAllowed').setValue(bEditAllowed);
	
	//Build List of Names
	var ndb:NotesDatabase = GetDB_Database("USER");
	var nv:NotesView = ndb.getView("USER_Interpreters");
	var nvec:NotesViewEntryCollection;
	var vNamesSelected = docX.getItemValue('Planner_Interpreter_Selected');
		
	if (IsNullorBlank(vNamesSelected)){
		nvec = nv.getAllEntries();
	} else {
		nvec = nv.getAllEntriesByKey('ABCDEF'); //Blank Collection		
		for(var i=0; i<vNamesSelected.length; i++) {
			nvec.addEntry(nv.getEntryByKey(vNamesSelected[i], true))
		}	
	}
	
	if(nvec.getCount() == 0){
		docX.replaceItemValue('Planner_HTML', '<div class="alert alert-danger"><strong>No Records found</strong></div>');
		return
	}
	
	Planner_BuildMap(nvec);
	
	var strHTML = Planner_HTML();		
	if(IsNullorBlank(strHTML)){
		view.postScript("Notify_Warning('Error in generating report, please contact IT Support')"); 
		return
	} else {
		docX.replaceItemValue('Planner_HTML', strHTML);		
	}	
}

function Planner_BuildMap(nvec:NotesViewEntryCollection){
	
	var vHM_Hours:java.util.HashMap = USER_Hours_Standard();
	var vHM_Hours_NS:java.util.HashMap = USER_Hours_NonStandard();
	var nv_Leaves:NotesView = GetDB_Database('USER').getView('USER_LEAV_Planner');
	var nv_ExHrs:NotesView = GetDB_Database('USER').getView('USER_HRS_DT_ByKey');
	
	//Key is the Name of Interpreter and Value will be Object
	var mapInterpreter:java.util.TreeMap = new java.util.TreeMap();
	
	var nvec:NotesViewEntryCollection = nvec;
	var nve:NotesViewEntry = nvec.getFirstEntry();
	var vLunchConfig = GetKeyword('Planner_LunchTime_Setup');
	
	var vHolidays = GetKeyword('Holidays_' + I18n.toString(sessionScope.get(gbl_Planner_SCKey_Date)[0], "yyyy"));
			
	while (nve != null){
		
		var ndoc:NotesDocument = nve.getDocument();
		//var strName = ndoc.getItemValueString('USER_FullName');
		var strName = ndoc.getItemValueString('User_LastName') + ", " + ndoc.getItemValueString('User_FirstName');
		var strDocKey = ndoc.getItemValueString('DocKey');		
		var vInterpreterData = Planner_Build_InterpreterData(strDocKey, nv_Leaves, nv_ExHrs, vHM_Hours, vHM_Hours_NS, vLunchConfig, vHolidays);
		
		mapInterpreter.put(strName, vInterpreterData)
		nve = nvec.getNextEntry(nve);		
	}
	
	sessionScope.put(gbl_Planner_SCKey, mapInterpreter);
}

function Planner_Build_InterpreterData(strDocKey, nv_Leaves, nv_ExHrs, vHM_Hours, vHM_Hours_NS, vLunchConfig, vHolidays){
	
	//Parameters if not passed to this function
	if(!nv_Leaves){var nv_Leaves:NotesView = GetDB_Database('USER').getView('USER_LEAV_Planner')}
	if(!nv_ExHrs){var nv_ExHrs:NotesView = GetDB_Database('USER').getView('USER_HRS_DT_ByKey')}
	if(!vHM_Hours){var vHM_Hours:java.util.HashMap = USER_Hours_Standard()}
	if(!vHM_Hours_NS){var vHM_Hours_NS:java.util.HashMap = USER_Hours_NonStandard()}
	if(!vLunchConfig){var vLunchConfig = GetKeyword('Planner_LunchTime_Setup')}
	if(!vHolidays){var vHolidays = GetKeyword('Holidays_' + I18n.toString(sessionScope.get(gbl_Planner_SCKey_Date)[0], "yyyy"))}
		
	var vData = []; var totHours = 0; var dataRow; var dt:Date; var intWD; var bAnySplitShift = false;
	var vDates:java.util.Vector = sessionScope.get(gbl_Planner_SCKey_Date);
	
	for(var i= 0; i <vDates.size(); i++){
		
		dataRow = {};
		dt = vDates.elementAt(i);
		intWD = @Weekday(dt); //WeekDay
		
		var vValues = vHM_Hours.get(intWD);	//Standard Hours
		if(vHM_Hours_NS.containsKey(strDocKey)){		
			vValues = vHM_Hours_NS.get(strDocKey)[intWD-1];
		}
		
		dataRow.leave = false;
		dataRow.dayoff = false;
		dataRow.publicHoliday = false;
		dataRow.extraHrs = false;
		dataRow.splitHours = false;
		dataRow.startTime_2 = 0;
		
		//Check Public Holiday
		if(vHolidays.indexOf(I18n.toString(dt, "dd/MM/yyyy"))>=0){
			vValues = 0; dataRow.publicHoliday = true;
		
		//Check Leave, if any set as 0	
		} else if(Planner_Interpreter_CheckLeaveDay(strDocKey, dt, nv_Leaves) == true){
			vValues = 0; dataRow.leave = true;
			
		} else {
			
			//Interpreter may have worked on day off, so check for extra hours
			var vValues_New = Planner_Interpreter_CheckHours(strDocKey, dt, vValues, nv_ExHrs);
			
			//if vValues is still same, then it means no extra hours
			dataRow.extraHrs = (vValues != vValues_New);
			vValues = vValues_New;
			dataRow.dayoff = (vValues == 0); //Value still zero means dayOff
			
			if(vValues != 0){
				dataRow.lunchFlag = 'A';	//Default
				if(vValues.length > 2){
					dataRow.lunchFlag = vValues[2];
				}
			}	
		}
		
		if(vValues == 0){
			dataRow.startTime_1 = 0; dataRow.endTime_1 = 0; dataRow.hours_1 = 0; dataRow.lunch = 0; dataRow.total = 0; 				
		} else {
			
			var strValueStart = @Right('0' + @Text(vValues[0]), 4);
			dataRow.startTime_1 = @Left(strValueStart, 2) + ":" + @Right(strValueStart, 2);
			
			var strValueEnd = @Right('0' + @Text(vValues[1]), 4);
			dataRow.endTime_1 = @Left(strValueEnd, 2) + ":" + @Right(strValueEnd, 2); 
			
			//No. of Hours
			dataRow.hours_1 = Planner_TimeDifference(dt.toDateString(), dataRow.startTime_1, dataRow.endTime_1);
			var intHoursCheck = dataRow.hours_1;
			dataRow.total = @TextToNumber(dataRow.hours_1);
			
			//Split Working Shift Case
			if(vValues.length > 3){
				bAnySplitShift = true;
				dataRow.splitHours = true;
				strValueStart = @Right('0' + @Text(vValues[3]), 4);
				dataRow.startTime_2 = @Left(strValueStart, 2) + ":" + @Right(strValueStart, 2);
				
				strValueEnd = @Right('0' + @Text(vValues[4]), 4);
				dataRow.endTime_2 = @Left(strValueEnd, 2) + ":" + @Right(strValueEnd, 2); 
								
				//No. of Hours
				dataRow.hours_2 = Planner_TimeDifference(dt.toDateString(), dataRow.startTime_2, dataRow.endTime_2);
				//Get the bigger hours difference here to compare
				if(dataRow.hours_2 > intHoursCheck){intHoursCheck = dataRow.hours_2}
				
				dataRow.total = dataRow.total + @TextToNumber(dataRow.hours_2);				
			} 
			
			dataRow.lunch = 0;
			
			if((intHoursCheck > @TextToNumber(vLunchConfig[0]) & dataRow.lunchFlag == 'A') | dataRow.lunchFlag == 'Y'){
				dataRow.lunch = @TextToNumber(vLunchConfig[1]).toFixed(2);
				dataRow.total = (dataRow.total - @TextToNumber(vLunchConfig[1]));
			}
			
			totHours = totHours + dataRow.total;
			dataRow.total = dataRow.total.toFixed(2);
		}
		
		vData.push(dataRow);			
	}
	
	var vInterpreterData = {};
	vInterpreterData.data = vData;
	vInterpreterData.total = totHours.toFixed(2);
	vInterpreterData.dockey = strDocKey;
	vInterpreterData.splitShift = bAnySplitShift;
	
	return vInterpreterData;
}

function Planner_HTML(){
	
	var dataRow;	
	var strStyle_Leave = 'style="text-align: center; vertical-align: middle; background-color: #ffecec"';
	var strStyle_DayOff = 'style="text-align: center; vertical-align: middle; background-color: #e2e2e2"';
	var strStyle_ExHrs = 'style="background-color: #e2ffe2"';
		
	var strHTML = '<table class="table table-bordered table-condensed table-striped" id="tblPlanner"><thead><tr class="info">';
	strHTML += '<th rowspan="2" style="vertical-align: middle;">#</th>';
	strHTML += '<th rowspan="2" style="vertical-align: middle;">Interpreter Name</th>';
	
	var vDates:java.util.Vector = sessionScope.get(gbl_Planner_SCKey_Date);
	var vDates_JS_ID = [];
	for(var i= 0; i <vDates.size(); i++){
		strHTML += '<th colspan="3" class="text-center">' + I18n.toString(vDates.elementAt(i) , "EEE, dd-MMM-yy") + '</th>';
		vDates_JS_ID.push(I18n.toString(vDates.elementAt(i) , "dd-MMM-yyyy"))
	}
	
	strHTML += '<th rowspan="2" class="text-right">Total</th>';	
	strHTML += '</tr><tr class="info">';
	
	for(var i= 0; i <vDates.size(); i++){
		strHTML += '<th class="planner_hours">Start</th><th class="planner_hours">End</th><th class="planner_hours">Hours</th>';
	}	
	strHTML += '</tr></thead>';
	
	var strStyle;
	var mapInterpreter:java.util.TreeMap = sessionScope.get(gbl_Planner_SCKey);
	var set:java.util.Set = mapInterpreter.entrySet(); 
    var iterator:java.util.Iterator = set.iterator();
	var counter = 1;
	
    while(iterator.hasNext()) {
		
		var entry = iterator.next();
		
		var strInterperter = entry.getKey();
		var vData = entry.getValue().data;
		var bAnySplitShift = entry.getValue().splitShift;
		
		strHTML += '<tr><td style="vertical-align: middle;">' + counter + '.</td>';
		strHTML += '<td style="vertical-align: middle;">' + strInterperter + '</td>';
		
		for(var i=0; i<vData.length; i++){			
			
			dataRow = vData[i];
			
			var strID = 'TDH~' + entry.getValue().dockey + '~' + vDates_JS_ID[i] + '~';	//TDH~DocKey~Date~
			
			if(dataRow.startTime_1 != 0){
				
				strID += (dataRow.extraHrs ? 'Edit' : 'Add');		//TDH~DocKey~Date~Action~
				strID += ('~' + dataRow.startTime_1 + '~' + dataRow.endTime_1 + '~' + dataRow.lunchFlag);	//TDH~DocKey~Date~Action~Start_1~End_1~LunchFlag
				if(dataRow.startTime_2 != 0){
					strID += ('~' + dataRow.startTime_2 + '~' + dataRow.endTime_2);	//TDH~DocKey~Date~Action~Start_1~End_1~LunchFlag~~Start_2~End_2
				}
				
				var strStyle = (dataRow.extraHrs ? strStyle_ExHrs : '');
				
				strHTML += '<td id="' + strID + '" colspan="3" style="padding:0px" class="' + strInterperter + '">';
				strHTML += '<div class="planner_hour_table" ' + strStyle + '><table>';
				strHTML += '<tr><td class="planner_hours">' + dataRow.startTime_1 + '</td>';
				strHTML += '<td class="planner_hours">' + dataRow.endTime_1 + '</td>';
				strHTML += '<td class="planner_hours">' + dataRow.hours_1 + '</td></tr>';
				
				if(dataRow.startTime_2 != 0){
					strHTML += '<tr><td class="planner_hours">' + dataRow.startTime_2 + '</td>';
					strHTML += '<td class="planner_hours">' + dataRow.endTime_2 + '</td>';
					strHTML += '<td class="planner_hours">' + dataRow.hours_2 + '</td></tr>';
				} else {
					//Generate blank row to balance table height
					if(bAnySplitShift){
						strHTML += '<tr><td class="planner_hours">0</td>';
						strHTML += '<td class="planner_hours">0</td>';
						strHTML += '<td class="planner_hours">0</td></tr>';
					}
				}
				
				strHTML += '<tr><td colspan="2" class="planner_hours">Lunch</td>';
				strHTML += '<td	class="planner_hours">' + dataRow.lunch + '</td></tr>';
				strHTML += '<tr><td colspan="2" class="planner_hours">Day Total</td>';
				strHTML += '<td class="planner_hours">' + dataRow.total + '</td></tr></table></div></td>';
							
			} else {
				
				if(dataRow.publicHoliday == true){	
					strHTML += '<td colspan="3" ' + strStyle_Leave + '>Public Holiday</td>';
					
				} else if(dataRow.leave == true){					
					strHTML += '<td colspan="3" ' + strStyle_Leave + '>On Leave</td>';				
				
				} else if(dataRow.dayoff == true & dataRow.extraHrs == false){
					strID += 'Add';	//TDH~DocKey~Date~Action
					strHTML += '<td id="' + strID + '" colspan="3" ' + strStyle_DayOff + ' class="' + strInterperter + '">Day Off</td>';				
				}
			}
		}
		
		strHTML += '<td class="text-right" style="vertical-align: bottom;">' + entry.getValue().total + '</td></tr>';
		counter++;
    }
    
    strHTML += '</table>';
	return strHTML;
}

function Planner_Interpreter_CheckLeaveDay(strDocKey, dt:Date, nv_Leaves){
	
	var intStart = parseFloat(I18n.toString(dt, "yyyyMMdd"));	
	var nvec:NotesViewEntryCollection = nv_Leaves.getAllEntriesByKey(strDocKey, true);
	if(nvec.getCount()>0){	
		var nve:NotesViewEntry = nvec.getFirstEntry();				
		while(nve != null){	
			vLeaves = @TextToNumber(nve.getColumnValues().elementAt(1).split('~'));
			if((intStart >= vLeaves[0] & intStart <= vLeaves[1])){
				return true;
			}		
			nve = nvec.getNextEntry(nve);
		}			
	}	
	return false	
}

function Planner_Interpreter_CheckHours(strDocKey, dt:Date, vValues, nv_ExHrs){
	
	var ndoc:NotesDocument = nv_ExHrs.getDocumentByKey(strDocKey + "~" + I18n.toString(dt, "yyyyMMdd"), true)
	if (ndoc == null){return vValues}	
	
	var vValues_New = [];
	vValues_New.push(parseFloat(ndoc.getItemValueString('HRS_DT_Start_1')));
	vValues_New.push(parseFloat(ndoc.getItemValueString('HRS_DT_End_1')));
	vValues_New.push(ndoc.getItemValueString('HRS_DT_Lunch'));
	if(ndoc.getItemValueString('HRS_DT_Split') == 'Y'){
		vValues_New.push(parseFloat(ndoc.getItemValueString('HRS_DT_Start_2')));
		vValues_New.push(parseFloat(ndoc.getItemValueString('HRS_DT_End_2')));
	}
	return vValues_New	
}

function Planner_Interpreter_AddEdit(){	
	var strDocKey = getComponent('scPlanner_InterperterDocKey').getValue();
	var strInterpreter = getComponent('scPlanner_InterperterName').getValue();
	var strResult = USER_HRS_DT_AddEdit(getComponent('scPlanner_Date').getValue(), strDocKey, getComponent('fld_Lunch').getValue());	
	Planner_Action_OnComplete(strInterpreter, strDocKey);
}

function Planner_Interpreter_Delete(){
	
	var strDocKey = getComponent('scPlanner_InterperterDocKey').getValue();
	var strInterpreter = getComponent('scPlanner_InterperterName').getValue();
	var strDate = getComponent('scPlanner_Date').getValue();
	var ndtS:NotesDateTime = session.createDateTime(strDate);
	
	var strFormula = 'Form="USER_HRS_DT" & DocKey_Parent="' + strDocKey + '" & HRS_DT = "' + I18n.toString(ndtS.toJavaDate(), 'yyyyMMdd') + '"';
	var dc:NotesDocumentCollection = GetDB_Database('USER').search(strFormula, null, 0)
	if(dc.getCount() == 0){
		print('ERROR: Document not found with Formula : ' + strFormula)
		return 'ERROR'
	}
	var docHR:NotesDocument = dc.getFirstDocument();
	Global_RemoveDocument(docHR);
	
	//Update Application Scope
	USER_Hours_Extra(true);
	
	Planner_Action_OnComplete(strInterpreter, strDocKey);
}

function Planner_Action_OnComplete(strInterpreter, strDocKey){
	
	var mapInterpreter:java.util.TreeMap = sessionScope.get(gbl_Planner_SCKey);
	mapInterpreter.remove(strInterpreter);
	mapInterpreter.put(strInterpreter, Planner_Build_InterpreterData(strDocKey))
	
	sessionScope.remove(gbl_Planner_SCKey);
	sessionScope.put(gbl_Planner_SCKey, mapInterpreter);
	
	var strHTML = Planner_HTML();		
	if(IsNullorBlank(strHTML)){
		view.postScript("Notify_Warning('Error in generating report, please contact IT Support')"); 
		return
	} else {
		document1.replaceItemValue('Planner_HTML', strHTML);		
	}	
}

function Planner_TimeDifference(strDate, strStartTime, strEndTime){	
	var ndtS:NotesDateTime = session.createDateTime(strDate + " " + strStartTime);
	var ndtE:NotesDateTime = session.createDateTime(strDate + " " + strEndTime);
	return (ndtE.timeDifference(ndtS)/3600).toFixed(2);
}