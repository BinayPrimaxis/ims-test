package com.primaxis;

import lotus.domino.Database;
import lotus.domino.NotesException;
import lotus.domino.Session;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

public class ViewColumn {

	private OpenLogItem oli;
	private String rowSeparater = "~!~";
	private String colSeparater = "~@~";

	public ViewColumn(Session session) {

		try {

			Database thisdb = session.getCurrentDatabase();

			String strFormula = "@DbLookup( \"\" : \"NoCache\" ; \"\" ; \"kwd_byName\" ; \"DB_FilePath_OpenLog\" ; 2 ; [FAILSILENT] )";
			String openLogDbPath = session.evaluate(strFormula).get(0)
					.toString().replace("\\", "\\\\");

			oli = new OpenLogItem(thisdb.getFilePath(), "ViewColumn.java");
			oli.useCustomLogDb(session.getDatabase(thisdb.getServer(),
					openLogDbPath, false));

		} catch (NotesException e) {
			System.out
					.println("ERROR: PRIMAXIS Support, please fix - Cannot get LOG DATABASE in java");
			e.printStackTrace();
		}

	}

	public String getViewColumnValueJSON(lotus.domino.View objView) {

		String strReturnValue = "[]";

		try {

			objView.setAutoUpdate(false);
			String strTemp = buildJson(objView.createViewNav(), true);
			if (strTemp.length() > 0) {
				strReturnValue = strTemp;
			}
			objView.setAutoUpdate(true);

		} catch (NotesException e) {
			oli.logError(e);
		}

		return strReturnValue;
	}
	
	public String getViewColumnValueJSON_Positions(lotus.domino.View objView,
			Integer posStart, Integer posEnd) {

		ViewNavigator nav = null;
		StringBuilder json = new StringBuilder();
		String strReturnValue = "[]";

		try {

			objView.setAutoUpdate(false);

			nav = objView.createViewNav();
			nav.setBufferMaxEntries(400);
			ViewEntry entry = nav.getCurrent();

			int pos = (entry.getColumnValues().size() - 1);
			nav.gotoPos(Integer.toString(posStart), '.');
			entry = nav.getCurrent();

			for (int i = 0; i <= (posEnd - posStart); i++) {

				json.append(colSeparater
						+ entry.getColumnValues().elementAt(pos).toString()
						+ rowSeparater);
				ViewEntry tmpentry = nav.getNext(entry);
				entry.recycle();
				entry = tmpentry;
			}

			objView.setAutoUpdate(true);

			strReturnValue = JSON_Construct(json.toString());

		} catch (NotesException e) {
			oli.logError(e);
		}

		return strReturnValue;
	}

	public String getViewColumnValueJSON_Positions_Calendar(
			lotus.domino.View objView, Integer posStart, Integer posEnd) {

		ViewNavigator nav = null;
		StringBuilder json = new StringBuilder();
		String strReturnValue = "[]";

		try {

			objView.setAutoUpdate(false);

			nav = objView.createViewNav();
			nav.setBufferMaxEntries(400);
			ViewEntry entry = nav.getCurrent();

			int pos = (entry.getColumnValues().size() - 1);
			nav.gotoPos(Integer.toString(posStart), '.');
			entry = nav.getCurrent();

			for (int i = 0; i <= (posEnd - posStart); i++) {

				json.append(entry.getColumnValues().elementAt(pos).toString());
				ViewEntry tmpentry = nav.getNext(entry);
				entry.recycle();
				entry = tmpentry;
			}

			objView.setAutoUpdate(true);

			// Remove last RowSeparater
			strReturnValue = json.toString();
			strReturnValue = "["
					+ strReturnValue.substring(0, strReturnValue
							.lastIndexOf(",")) + "]";

		} catch (NotesException e) {
			oli.logError(e);
		}

		return strReturnValue;
	}

	public String getViewColumnValueJSON_FTSearch(lotus.domino.View objView,
			String query, int intPos, boolean addEmptyCol) {

		String strReturnValue = "[]";

		try {
			if (objView.FTSearch(query, 0) > 0) {

				lotus.domino.ViewEntryCollection objVEC = objView
						.getAllEntries();
				StringBuilder jsonVE = new StringBuilder();
				ViewEntry entry = objVEC.getFirstEntry();

				if (intPos == -1) {
					intPos = (entry.getColumnValues().size() - 1);
				}
			
				while (entry != null) {
					if (addEmptyCol) {
						jsonVE.append(colSeparater);
					}
					jsonVE.append(entry.getColumnValues().elementAt(intPos)
							.toString());
					jsonVE.append(rowSeparater);
					ViewEntry tmpentry = objVEC.getNextEntry(entry);
					entry.recycle();
					entry = tmpentry;
				}
				objVEC.recycle();

				if (jsonVE.length() > 0) {
					// strReturnValue = jsonVE.toString();
					strReturnValue = JSON_Construct(jsonVE.toString());
				}

			}

		} catch (NotesException e) {
			oli.logEvent("Query - " + query, "0", null);
			oli.logError(e);
			System.out.println("ID - " + String.valueOf(e.id) + " , TEXT - "
					+ e.text);
		}

		return strReturnValue;
	}

	public String getViewColumnValue_TypeAhead(lotus.domino.View objView,
			String query, int intPos, int intMaxDocs) {

		String strValue = "[]";

		try {
			if (objView.FTSearch(query, intMaxDocs) > 0) {

				lotus.domino.ViewEntryCollection objVEC = objView
						.getAllEntries();
				StringBuilder jsonVE = new StringBuilder();
				ViewEntry entry = objVEC.getFirstEntry();

				if (intPos == -1) {
					intPos = (entry.getColumnValues().size() - 1);
				}

				while (entry != null) {
					jsonVE.append(entry.getColumnValues().elementAt(intPos)
							.toString());
					jsonVE.append(rowSeparater);
					ViewEntry tmpentry = objVEC.getNextEntry(entry);
					entry.recycle();
					entry = tmpentry;
				}
				objVEC.recycle();

				if (jsonVE.length() > 0) {
					strValue = jsonVE.toString();
					strValue = JSON_Handle_SpecialCharacters(strValue);
					strValue = strValue.substring(0, strValue
							.lastIndexOf(rowSeparater));
					strValue = "[\""
							+ strValue.replaceAll(rowSeparater, "\",\"")
							+ "\"]";
				}

			}

		} catch (NotesException e) {
			oli.logEvent("Query - " + query, "0", null);
			oli.logError(e);
			System.out.println("ID - " + String.valueOf(e.id) + " , TEXT - "
					+ e.text);
		}

		return strValue;
	}

	public String getViewColumnValueJSON_Category(lotus.domino.View objView,
			String[] vCat) {

		StringBuilder json = new StringBuilder();
		String strReturnValue = "[]";

		try {

			objView.setAutoUpdate(false);

			for (int i = 0; i < vCat.length; i++) {
				ViewNavigator vn = objView.createViewNavFromCategory(vCat[i]);
				json.append(buildJson(vn, false));
			}

			objView.setAutoUpdate(true);
			if (json.length() > 0) {
				strReturnValue = JSON_Construct(json.toString());
			}

		} catch (NotesException e) {
			oli.logError(e);
		}

		return strReturnValue;
	}

	private String buildJson(lotus.domino.ViewNavigator objNav,
			boolean bConstructJson) {

		StringBuilder jsonVE = new StringBuilder();
		String strValue = "";

		try {

			if (objNav.getCount() > 0) {

				objNav.setBufferMaxEntries(400);
				ViewEntry entry = objNav.getCurrent();
				int pos = (entry.getColumnValues().size() - 1);

				while (entry != null) {
					jsonVE.append(colSeparater); // This is for Grid Json Map
					jsonVE.append(entry.getColumnValues().elementAt(pos)
							.toString());
					jsonVE.append(rowSeparater);
					ViewEntry tmpentry = objNav.getNext(entry);
					entry.recycle();
					entry = tmpentry;
				}

				objNav.recycle();
				if (jsonVE.length() > 0) {
					if (bConstructJson) {
						strValue = JSON_Construct(jsonVE.toString());
					} else {
						strValue = jsonVE.toString();
					}
				}
			}

		} catch (NotesException e) {
			oli.logError(e);
		}

		return strValue;
	}

	public String JSON_Construct(String data) {

		data = JSON_Handle_SpecialCharacters(data);

		// Replace ColSeparate and Quotes and comma - "item1", "item2"
		data = "\"" + data.replaceAll(colSeparater, "\",\"") + "\"";

		// Remove last RowSeparater
		data = data.substring(0, data.lastIndexOf(rowSeparater));

		// replace all row separater with "],["
		data = data.replaceAll(rowSeparater, "\"],[\"");

		data = "[[" + data + "\"]]";

		return data;

	}

	public String JSON_Handle_SpecialCharacters(String data) {

		// Replace single forward slash with double forward slash
		data = data.replaceAll("\\\\", "\\\\\\\\");

		// Prefix foward slash to double quotes
		data = data.replaceAll("\"", "\\\\\"");

		// Fix new line/carriage returns
		// data = data.replaceAll("(\\r|\\n|\\r\\n)+", "\\\\n");
		data = data.replaceAll("(\\r)+", "\\\\r");
		data = data.replaceAll("(\\n)+", "\\\\n");
		// data = data.replaceAll("(\\r|\\n|\\r\\n)+", "<br>");

		// fix tabs
		data = data.replaceAll("\\t", " ");

		return data;
	}
		
}