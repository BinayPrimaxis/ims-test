package com.primaxis;

import java.util.Hashtable;
import java.util.List;
import java.util.ArrayList;
import java.util.Vector;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;

import lotus.domino.*;

@SuppressWarnings("unchecked")
public class LDAPConnectAD {

	private static Database thisDB;
	private static View nvKW;
	private static LdapContext ldapContext;

	public LDAPConnectAD(Session session) throws NotesException {

		try {

			thisDB = session.getCurrentDatabase();
			nvKW = thisDB.getView("KWD_byName");
			// cLog(nvKW.getName());

		} catch (NotesException e) {
			e.printStackTrace();
		}

		try {
			Hashtable<String, String> htbl = new Hashtable<String, String>();
			htbl.put(Context.INITIAL_CONTEXT_FACTORY,
					"com.sun.jndi.ldap.LdapCtxFactory");
			htbl.put(Context.PROVIDER_URL, GetKeyword("LDAP_URL").elementAt(0)
					.toString());
			htbl.put(Context.URL_PKG_PREFIXES, "com.sun.jndi.url");
			htbl.put(Context.REFERRAL, "follow");
			htbl.put(Context.SECURITY_AUTHENTICATION, "simple");
			htbl.put(Context.SECURITY_PRINCIPAL, GetKeyword("LDAP_User")
					.elementAt(0).toString());
			htbl.put(Context.SECURITY_CREDENTIALS, GetKeyword("LDAP_UserPwd")
					.elementAt(0).toString());

			ldapContext = new InitialLdapContext(htbl, null);

			// cLog("ldapContext - Complete");

		} catch (NamingException e) {
			throw new RuntimeException(e);
		}

	}

	public static List<List<String>> SearchUser(String strSearch) {

		cLog("SearchUser Start, searching for -" + strSearch);
		List<List<String>> UserList = new ArrayList<List<String>>();
		Vector<String> AttList = GetKeyword("LDAP_FieldList");

		try {
			ldapContext.setRequestControls(null);

			NamingEnumeration<?> namingEnum = ldapContext.search("", strSearch,
					getSimpleSearchControls());

			while (namingEnum.hasMoreElements()) {
				SearchResult result = (SearchResult) namingEnum.next();
				Attributes attrs = result.getAttributes();
				//cLog(result.getNameInNamespace());
				//cLog(attrs.get("distinguishedName").get().toString());

				List<String> userObj = new ArrayList<String>();
				int errorCount = 0;

				for (int i = 0; i < AttList.size(); i++) {
					
					String strAttName = AttList.elementAt(i);
					if (attrs.get(strAttName) != null) {
						String strAttValue = attrs.get(strAttName).get()
								.toString();
						userObj.add(strAttValue);
					} else {
						cLog("ERROR: Attribute: '" + strAttName
								+ "' not available for -> " + attrs.get("cn"));
						userObj.add("ERROR");
						errorCount++;
					}
				}
				if (errorCount > 2) {
					cLog("ERROR: more than 2 errors in attributes, "
							+ attrs.get("cn") + ", not added to list");
				} else {
					UserList.add(userObj);
				}
			}

			namingEnum.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		cLog("Total Users : " + UserList.size() + "");
		return UserList;
	}

	private static SearchControls getSimpleSearchControls() {
		SearchControls searchControls = new SearchControls();
		searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		searchControls.setTimeLimit(30000);
		return searchControls;
	}

	private static Vector<String> GetKeyword(final String strKeyName) {

		Vector<String> vValues = null;

		try {
			Document kDoc = nvKW.getDocumentByKey(strKeyName, true);
			if (kDoc != null) {
				if (kDoc.getItemValueString("KWType").equalsIgnoreCase("Text")) {
					vValues = kDoc.getItemValue("KWText");
				} else {
					vValues = kDoc.getItemValue("KWMultiText");
				}
				kDoc.recycle();
			} else {
				cLog("Keyword document not found : " + strKeyName);
			}

		} catch (NotesException e) {
			e.printStackTrace();
		}

		return vValues;
	}

	private static void cLog(String strMsg) {
		System.out.println("~~~~LDAPConnect -->" + strMsg);
	}
}
